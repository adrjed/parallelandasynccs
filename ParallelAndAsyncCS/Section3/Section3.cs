﻿using AJ.Reflection;
using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace ParallelAndAsyncCS.Section3
{
    [CInf(3, 1, "Zmienne w aplikacjach wielowątkowych")]
    public class Section3_1
    {
        //po usunięciu znaków komentarza każdy wątek będzie miał własną kopię tej zmiennej
        [ThreadStatic]
        static int licznik = 0;

        [FInf(3,1, "Prosty przykład użycia licznika w aplikacji wielowątkowej")]
        public void Main_3_1(/*string[] args*/)
        {
            WaitCallback metodaWatku = ( object parametr ) =>
                {
                    Interlocked.Increment( ref licznik ); // licznik++;
                    Console.Write( (int)parametr );
                    Console.WriteLine( "Wątek: " + Thread.CurrentThread.ManagedThreadId +
                                      ", licznik= " + licznik.ToString() );
                };
            for ( int i = 0 ;i < 4 ;i++ ) ThreadPool.QueueUserWorkItem( metodaWatku, i );                

            Console.ReadLine(); //wątek główny czeka na dodatkowe wątki
        }
    }
    [CInf(3, 2, "Opóźniona inicjacja i zmienne lokalne wątku")]
    public class Section_3_2
    {
        [FInf(3,2, "Ciało metody Main aplikacji konsolowej")]
        public void Main_3_2()
        {
            Lazy<int> li = new Lazy<int>( () => 1 ); // deklaracja zmiennej i wskazanie funkcji
            Console.WriteLine( "Czy utworzona? : {0}", li.IsValueCreated.ToString() ); // niezainicjowana
            Console.WriteLine( "Odwołanie do zmiennej, li={0}", li.Value ); // leniwa inicjacja
            Console.WriteLine( "Czy utworzona? : {0}", li.IsValueCreated.ToString() ); // już zainicjowana
            Console.ReadLine(); // wątek główny czeka na dodatkowe wątki
        }

        [FInf(3,3, "Użycie 'wraperra' Lazy<> w aplikacji Windows Forms")]
        public void Main_3_3()
        {
            Lazy<Button> lb = new Lazy<Button>(() =>
            {
                Button b = new Button();
                //b.Parent = this;
                b.Content = "Leniwy przycisk";
                return b;
            });
            MessageBox.Show( "Czy utworzona?: {0}", lb.IsValueCreated.ToString() );
            MessageBox.Show( "Odwołanie do zmiennej, etykieta przycisku: {0}", lb.Value.Content.ToString() );
            MessageBox.Show( "Czy utworzona? : {0}", lb.IsValueCreated.ToString() );
        }
    }

    [CInf(3, 4, "Opóźnione inicjowane zmiennej w aplikacji wielowątkowej")]
    public class Section_3_4
    {
        static Lazy<int> li = new Lazy<int>( () => Thread.CurrentThread.ManagedThreadId );

        [FInf(3, 4, "Inicjowanie zmiennej w wielu wątkach")]
        public void Main_3_4()
        {
            WaitCallback metodaWatku = ( object parametr ) =>
                {
                    Console.WriteLine( "Wątek: " + Thread.CurrentThread.ManagedThreadId.ToString() +
                                      ", Czy utworzona? : " + li.IsValueCreated.ToString() );// jeszcze niezainicjowana
                    Console.WriteLine("Wątek: " + Thread.CurrentThread.ManagedThreadId.ToString() + 
                                      ", li=" + li.Value.ToString()); // leniwa inicjacja
                    Console.WriteLine( "Wątek: " + Thread.CurrentThread.ManagedThreadId.ToString() +
                                       ", Czy utworzona? : " + li.IsValueCreated.ToString() );// już zainicjowana
                };
            for( int i = 0; i <  4; i++ )
            { ThreadPool.QueueUserWorkItem( metodaWatku, i ); }

            Console.ReadLine(); // wątek główny czeka na dodatkowe wątki
        }

        [FInf(3, 5, "Użycie klasy LazyInitializer do zapewnienia inicjacji obiektu")]
        public void button2_Click(/*object sender, EventArgs e*/)
        {
            // Button btn = new Button();
            Button btn = null;
            LazyInitializer.EnsureInitialized<Button>( ref btn, () =>
                {
                    Button b = new Button();
                    //b.Parent = this;
                    b.Margin = new Thickness( 100, 100, 0, 0 );
                    b.Content = "Leniwy przycisk";
                    return b;
                } );
            MessageBox.Show( "Odwołanie do zmiennej, etykieta przycisku: " + btn.Content.ToString() );
        }
    }

}
