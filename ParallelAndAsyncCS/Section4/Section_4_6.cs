﻿using AJ.Reflection;
using System;
using System.Threading;

namespace ParallelAndAsyncCS.Section4
{
    [CInf(4, "Barrier")]
    class Section_4_6
    {
        [FInf(4, 6, "Niezsynchronizowane drukowanie liczb")]
        public static void Main_4_6()
        {
            const int ThreadCount = 10;

            ThreadStart threadMethod = () =>
                {
                    for ( int i = 0 ;i < 10 ;++i )
                    {
                        Console.Write( i );
                    }
                };

            Thread[] threads = new Thread[ThreadCount];
            for (int i = 0; i < ThreadCount; ++i)
            {
                threads[i] = new Thread( threadMethod );
                threads[i].Start();
            }
            Console.ReadLine();
        }

        [FInf(4, 7, "Synchronizacja z użyciem Barrier")]
        public static void Main_4_7()
        {
            const int ThreadCount = 10;
            Barrier b = new Barrier( ThreadCount );
            // Zostanie wykonana dana instrukcja po osiągnięciu przez wszystkie wątki bariery
            Barrier b2 = new Barrier( ThreadCount, ( Barrier _b ) => { Console.WriteLine(); } );

            ThreadStart threadMethod = () =>
            {
                for ( int i = 0 ;i < 10 ;++i )
                {
                    Console.Write( i );
                    b.SignalAndWait();
                }
            };

            Thread[] threads = new Thread[ThreadCount];
            for ( int i = 0 ;i < ThreadCount ;++i )
            {
                threads[i] = new Thread( threadMethod );
                threads[i].Start();
            }
            Console.ReadLine();
        }
    }

    [CInf(4, "Mutex")]
    class Section_4_8
    {
        [FInf(4, 8, "Uzycie klasy Mutex do ograniczenia ilości instancji aplikacji")]
        public static void Main_4_8()
        {
            // kontrola ilości instancji aplikacji
            bool isFirstInstance;
            Mutex m = new Mutex( true, "UniqueMutexName", out isFirstInstance );
            if (isFirstInstance)
            {
                Console.WriteLine( "Pierwsza instancja" );
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine( "Instancja tego programu jest już uruchomiona. Program zostanie zamknięty!" );
                Console.ReadLine();
                return;
            }
            // dalsza część programu
            // ...
        }
    }

    [CInf(4, "Użycie obiektu klasy Mutex do utworzenia sekcji krytycznej o zasięgu 'międzyaplikacjowym'")]
    class Section_4_9
    {
        [FInf(4, 9, "Uruchomienie programu z obiektem Mutex")]
        public static void Main_4_9()
        {
            Console.WriteLine( "aplikacja została uruchomiona" );

            //kontrola ilości instancji aplikacji
            Mutex m = new Mutex( false, "BardzoUnikalnaNazwaMuteksu" );
            Console.WriteLine( "Muteks został utworzony" );
            Console.WriteLine();

            bool koniec = false;

            while(true)
            {
                m.WaitOne(); // czeka, kiedy będzie można wejść do sekcji krytycznej
                Console.Write( '[' );
                if (Console.KeyAvailable)
                {
                    switch(Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.Enter:
                            Console.WriteLine();
                            Console.WriteLine( "\n\nWątek został wstrzymany w sekcji krytycznej.\n Naciśniej Enter, aby zwolnić muteks..." );
                            Console.ReadLine();
                            break;
                        case ConsoleKey.Escape:
                            koniec = true;
                            break;
                    }
                }
                m.ReleaseMutex(); // zwalnia muteks (opuszcza sekcję krytyczną)
                Console.Write( "]" );
                if (koniec)
                {
                    Console.WriteLine( "\n\nKoniec" );
                    return;
                }
                Thread.Sleep( 1000 );
                Console.Write( " " );
            }
        }
    }

    [CInf(4, "Użycie klasy Semaphore i pokazanie różnic między Semaphor i Mutex")]
    class Section_4_10
    {
        [FInf(4,10, "Użycie klasy Semaphore")]
        public static void Main_4_10()
        {
            Console.WriteLine( "Aplikacja zostałą uruchomiona" );
            int criticalSectionThreadsCount = System.Environment.ProcessorCount;
            Console.WriteLine( "Ile wątków może być jednocześnie w sekcji krytycznej: " + criticalSectionThreadsCount );

            // kontrola ilości instancji aplikacji
            Semaphore s = new Semaphore( criticalSectionThreadsCount, criticalSectionThreadsCount, "BardzoUnikalnaNazwaSemafora" );
            Console.WriteLine( "Semafor został utworzony" );
            Console.WriteLine();

            bool koniec = false;

            while(true)
            {
                s.WaitOne(); // czeka, kiedy będzie można wejść do sekcji krytycznej
                Console.Write( "[" );
                if (Console.KeyAvailable)
                {
                    switch(Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.Enter:
                            Console.WriteLine();
                            Console.WriteLine( "\n\nWątek został wstrzymany w sekcji krytycznej.\n Naciśnij Enter, aby zwolnić semafor..." );
                            Console.ReadLine();
                            break;
                        case ConsoleKey.Escape:
                            koniec = true;
                            break;
                    }
                }
                int availableThreadsSpaceCount = s.Release();
                // zwalnie semafor (opuszcza sekcję krytyczną)
                Console.Write( availableThreadsSpaceCount + "]" );
                if (koniec)
                {
                    Console.WriteLine( "\n\nKoniec." );
                    return;
                }
                Thread.Sleep(1000);
                Console.Write( " " );
            }
        }
    }
}
