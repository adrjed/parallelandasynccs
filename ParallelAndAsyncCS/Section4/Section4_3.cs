﻿using AJ.Reflection;
using System;
using System.Threading;

namespace ParallelAndAsyncCS.Section4
{
    [CInf(4,3, "Przykład użycia klasy ReaderWriterLockSlim w scenariuszu czytelników i pisarzy")]
    class Section_4_3
    {
        static Random r = new Random();
        const int ileElem = 10;
        static int[] tablica = new int[ileElem];

        const int ileWatkowPisarzy = 2;
        const int ileWatkowCzytelnikow = 10;
        const int maxReadsBreak = 10000;    // 10s
        const int maxModifBreak = 10000;    // 10s
        const int maxReadLength = 1000;     // 1s
        const int maxModifLength = 100;     // 0.1s
        static ReaderWriterLockSlim rwls = new ReaderWriterLockSlim();

        private static void ModifyElem(int index, int? wartosc = null)
        {
            rwls.EnterWriteLock();
            Console.WriteLine( "Wątki czekające na zapis: {0}.\n Wątki czekające na odczyt: {1}.\n", rwls.WaitingWriteCount, rwls.WaitingReadCount );
            try
            {
                if ( wartosc.HasValue ) tablica[index] = wartosc.Value;
                else tablica[index]++;
                Console.WriteLine( "Element " + index.ToString() + " został zmieniony w wątku nr " + Thread.CurrentThread.ManagedThreadId );
                Thread.Sleep( r.Next( maxModifLength ) );
            }
            catch (Exception ex)
            {
                Console.WriteLine( "Modyfikacja elementu " + index.ToString() + " w wątku" + Thread.CurrentThread.ManagedThreadId + " nie jest możliwa (" + ex.Message + ")" );
            }
            finally
            {
                rwls.ExitWriteLock();
            }
        }

        private static int ReadElem(int index)
        {
            int wynik = -1;
            rwls.EnterReadLock();
            Console.WriteLine( "Wątki równocześnie odczytujące: {0}.\nWątki czekające na zapis: {1}", rwls.CurrentReadCount, rwls.WaitingWriteCount );
            try
            {
                wynik = tablica[index];
                Console.WriteLine( "Element " + index + " równy jest " + wynik + "\"" );
                Thread.Sleep( r.Next( maxReadLength ) );
                return wynik;
            }
            catch (Exception ex)
            {
                Console.WriteLine( "Odczyt elementu " + index + " nie jest możliwy (" + ex.Message + ")" );
                return wynik;
            }
            finally
            {
                rwls.ExitReadLock();
            }
        }

        private static void DisplayTable()
        {
            Console.WriteLine( "Zawartość tablicy: " );
            foreach ( int elem in tablica ) Console.WriteLine( elem.ToString() );
            Console.WriteLine( "[Koniec]" );
        }

        private int this[int index]
        {
            get { return ReadElem( index ); }
            set { ModifyElem( index, value ); }
        }

        [FInf(4, 3, "Wywołanie funkcji prezentującej działanie ReaderWriterLockSlim")]
        public static void Main_4_3()
        {
            DisplayTable();
            Console.WriteLine( "Naciśnij Enter..." );
            Console.WriteLine( "Następnie naciśniej Enter, jeżeli będziesz chciał zakończyć program." );
            Console.ReadLine();

            ThreadStart writerAction = () =>
                {
                    Thread.Sleep( r.Next( maxModifBreak ) );        // opóźnienie
                    while ( true ) 
                    {
                        try 
                        {
                            Console.WriteLine( "Przygotowanie do modyfikacji elementu (wątek nr " + Thread.CurrentThread.ManagedThreadId + ")" );
                            int index = r.Next( ileElem );
                            ModifyElem( index );
                        }
                        catch ( ThreadAbortException ) 
                        {
                            Console.WriteLine( "Wątek pisarza " + Thread.CurrentThread.ManagedThreadId + " kończy pracę" );
                        }
                    }
                };

            ThreadStart readerAction = () =>
                {
                    Thread.Sleep( r.Next( maxReadsBreak ) );        // opóźnienie
                    while ( true )
                    {
                        try
                        {
                            Console.WriteLine( "Przygotowania do odczytania elementu (wątek nr " + Thread.CurrentThread.ManagedThreadId + ")" );
                            int index = r.Next( ileElem );
                            int elemValue = ReadElem( index );
                            Console.WriteLine( "Odczytany element o indeksie " + index + " równy jest " + elemValue + " (wątek nr " + Thread.CurrentThread.ManagedThreadId + ")" );
                            Thread.Sleep( maxReadsBreak );
                        }
                        catch ( Exception ex )
                        {
                            Console.WriteLine( "Wątek czytelnika " + Thread.CurrentThread.ManagedThreadId + " kończy pracę - " + ex.Message );
                        }
                    }
                };

            Thread[] writersThr = new Thread[ileWatkowPisarzy];
            for( int i=0; i < ileWatkowPisarzy; ++i)
            {
                writersThr[i] = new Thread( writerAction );
                //writersThr[i].Priority = ThreadPriority.AboveNormal;
                writersThr[i].IsBackground = true;
                writersThr[i].Start();
            }

            Thread[] readersThr = new Thread[ileWatkowCzytelnikow];
            for ( int i = 0 ;i < ileWatkowCzytelnikow ;++i )
            {
                readersThr[i] = new Thread( readerAction ) { IsBackground = true };
                //readersThr[i].Priority = ThreadPriority.AboveNormal;
                readersThr[i].Start();
            }

            Console.ReadLine();
            Console.WriteLine( "\nKończenie pracy programu..." );
            foreach ( Thread t in writersThr ) t.Abort();
            foreach ( Thread t in readersThr ) t.Abort();

            DisplayTable();
        }
    }
}
