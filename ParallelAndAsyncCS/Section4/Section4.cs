﻿using AJ.Reflection;
using System;
using System.Threading;

/// <summary>Więcej o synchronizacji wątków. Blokady i sygnały.</summary>
namespace ParallelAndAsyncCS.Section4
{

    [CInf(4, 1, "Klasyczny przykład operacji bankowych")]
    class Konto
    {
        private decimal saldo;
        private int id;

        public Konto(decimal saldoPoczatkowe, int id)
        {
            saldo = saldoPoczatkowe;
            this.id = id;
        }

        public void Wyplata(decimal kwota)
        {
            saldo -= kwota;
            //Console.WriteLine( "Nastąpiła wypłata z konta {0} kwoty {1}. Saldo po operacji {2}", id, kwota, saldo );
        }

        public void Wplata(decimal kwota)
        {
            saldo += kwota;
            //Console.WriteLine( "Nastąpiła wpłata na konto {0} kwoty {1}. Saldo po operacji {2}", id, kwota, saldo );
        }

        public static void Przelew_4_1(Konto kontoPlatnika, Konto kontoOdbiorcy, decimal kwota)
        {
            if ( kontoOdbiorcy == kontoPlatnika ) throw new ArgumentException( "Niemożliwe jest wykonanie przelewu na to samo konto" );

            Console.WriteLine( "Przygotowanie do przelewu z konta {0} na konto {1} kwoty {2}.", kontoPlatnika.id, kontoOdbiorcy.id, kwota );
            Console.WriteLine( "Salda przed przelewem: konto {0} - saldo {1}, konto {2} - saldo {3}", kontoPlatnika.id, kontoPlatnika.saldo, kontoOdbiorcy.id, kontoOdbiorcy.saldo );

            lock (kontoPlatnika)
            {
                Console.WriteLine( "Dostep do konta płatnika {0} zarezerwowany", kontoPlatnika.id );
                Thread.Sleep( 1000 );
                lock (kontoOdbiorcy)
                {
                    Console.WriteLine("Dostep do konta odbiorcy {0} zarezerwowany", kontoOdbiorcy.id);
                    kontoPlatnika.Wyplata(kwota);
                    kontoOdbiorcy.Wplata(kwota);
                }
                Console.WriteLine("Dostęp do konta odbiorcy {0} zwolniony", kontoOdbiorcy.id);
            }

            Console.WriteLine("Dostęp do konta płatnika {0} zwolniony", kontoPlatnika.id);
        }

        public static void Przelew_4_2(Konto kontoPlatnika, Konto kontoOdbiorcy, decimal kwota)
        {
            if ( kontoOdbiorcy == kontoPlatnika ) throw new ArgumentException( "Niemożliwe jest wykonanie przelewu na to samo konto" );

            Konto kontoA, kontoB;
            if (kontoPlatnika.id < kontoOdbiorcy.id)
            {
                kontoA = kontoPlatnika;
                kontoB = kontoOdbiorcy;
            }
            else
            {
                kontoA = kontoOdbiorcy;
                kontoB = kontoPlatnika;
            }

            Console.WriteLine( "Przygotowanie do przelewu z konta {0} na konto {1} kwoty {2}.", kontoPlatnika.id, kontoOdbiorcy.id, kwota );
            Console.WriteLine( "Salda przed przelewem: konto {0} - saldo {1}, konto {2} - saldo {3}", kontoPlatnika.id, kontoPlatnika.saldo, kontoOdbiorcy.id, kontoOdbiorcy.saldo );

            lock(kontoA)
            {
                Console.WriteLine( "Dostęp do konta płatnika {0} zarezerwowany", kontoA.id );
                Thread.Sleep( 1000 );
                lock(kontoB)
                {
                    Console.WriteLine( "Dostęp do konta {0} zarezerwowany", kontoB.id );
                    kontoPlatnika.Wyplata( kwota );
                    kontoOdbiorcy.Wplata( kwota );
                }
                Console.WriteLine( "Dostęp do konta {0} zwolniony", kontoB.id );
            }
            Console.WriteLine( "Dostęp do konta {0} zwolniony", kontoA.id );

            Console.WriteLine( "Wykonany został przelew z konta {0} na konto {1}", kontoPlatnika.id, kontoOdbiorcy.id );
            Console.WriteLine( "Salda po przelewie: konto {0} - saldo {1}. konto {2} - saldo {3}", kontoPlatnika.id, kontoPlatnika.saldo, kontoOdbiorcy.id, kontoOdbiorcy.saldo );
        }
    }

    class PoleceniePrzelewu
    {
        public Konto KontoPlatnika;
        public Konto KontoOdbiorcy;
        public decimal Kwota;
    }

    [CInf(4, 1, "Klasyczny przykład operacji bankowych")]
    public class Section_4_1
    {
        [FInf(4, 1, "Klasyczny przykład operacji bankowych")]
        public void Main_4_1()
        {
            Konto k1 = new Konto( 100, 1 );
            Konto k2 = new Konto( 150, 2 );

            WaitCallback transakcja = ( object parametr ) =>
                {
                    PoleceniePrzelewu pp = parametr as PoleceniePrzelewu;
                    if ( pp == null ) throw new ArgumentNullException( "Brak polecenia przelewu" );
                    else Konto.Przelew_4_1( pp.KontoPlatnika, pp.KontoOdbiorcy, pp.Kwota );
                };

            ThreadPool.QueueUserWorkItem( transakcja, new PoleceniePrzelewu { KontoPlatnika = k1, KontoOdbiorcy = k2, Kwota = 50 } );
            ThreadPool.QueueUserWorkItem( transakcja, new PoleceniePrzelewu { KontoPlatnika = k1, KontoOdbiorcy = k2, Kwota = 10 } );

            Console.ReadLine(); // wątek główny czeka na dodatkowe wątki
        }
    }

    [CInf(4, 2, "Rozwiązanie problemu zakleszczenia dzięki wprowadzeniu kolejności zasobów")]
    public class Section_4_2
    {
        [FInf(4, 2, "Wprowadzenie kolejności zasobów w Przelew_4_2()")]
        public void Main_4_2()
        {
            Konto k1 = new Konto( 100, 1 );
            Konto k2 = new Konto( 150, 2 );

            WaitCallback transakcja = ( object parametr ) =>
                {
                    PoleceniePrzelewu pp = parametr as PoleceniePrzelewu;
                    if ( pp == null ) throw new ArgumentNullException( "Brak polecenia przelewu" );
                    else Konto.Przelew_4_2( pp.KontoPlatnika, pp.KontoOdbiorcy, pp.Kwota );
                };

            ThreadPool.QueueUserWorkItem( transakcja, new PoleceniePrzelewu { KontoPlatnika = k1, KontoOdbiorcy = k2, Kwota = 50 } );
            ThreadPool.QueueUserWorkItem( transakcja, new PoleceniePrzelewu { KontoPlatnika = k1, KontoOdbiorcy = k2, Kwota = 10 } );

            //Console.ReadLine();
        }
    }
}
