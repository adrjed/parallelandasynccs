﻿using AJ.ConsoleExtension;
using AJ.Reflection;
using System;
using System.Threading;

namespace ParallelAndAsyncCS.Section4
{
    [CInf(4, 4, "Kod ProducentKonsument z flagami")]
    class Section_4_4
    {
        static object magSyncObj = new object();
        static Random r = new Random();

        static volatile bool isProdThreadActive = true;
        static volatile bool isConsThreadActive = true;
        static Thread prodThread = null;
        static Thread consThread = null;

        const int maxProdTime = 1000;
        const int maxConsTime = 1000;
        const int maxProdLaunchTime = 5000;
        const int maxConsLaunchTime = 5000;

        static int magCapacity = 20;
        static int magElemAmount = 1;

        static void DisplayMagazineState()
        {
            Console.WriteLine( "Liczba elementów w magazynie: " + magElemAmount );
        }

        [FInf(4,4, "Uruchamianie szablonu ProducentKonsument")]
        public static void Main_4_4()
        {
            ThreadStart prodAction = () =>
                {
                    Console.WriteLine( "Wątek producenta jest uruchamiany" );
                    while ( true )
                    {
                        if ( isProdThreadActive )
                            Thread.Sleep( r.Next( maxProdLaunchTime ) );
                        while ( isProdThreadActive )
                        {
                            lock ( magSyncObj )
                            {
                                magElemAmount++;
                                ConsoleEx.WriteLine( "Element dodany.", ConsoleColor.Green );
                            }
                            DisplayMagazineState();
                            if ( magElemAmount >= magCapacity )
                            {
                                isProdThreadActive = false;
                                Console.WriteLine( "Wątek producenta został uśpiony" );
                            }
                            if ( !isConsThreadActive )
                            {
                                Console.WriteLine( "Wątek  konsumenta jest wznawiany" );
                                isConsThreadActive = true;
                            }
                            Thread.Sleep( r.Next( maxProdTime ) );
                        }
                    }
                };

            ThreadStart consAction = () =>
                {
                    Console.WriteLine( "Wątek konsumenta jest uruchamiany" );
                    while ( true )
                    {
                        if ( isConsThreadActive )
                            Thread.Sleep( r.Next( maxConsLaunchTime ) );
                        while ( isConsThreadActive )
                        {
                            lock ( magSyncObj )
                            {
                                magElemAmount--;
                                ConsoleEx.WriteLine( "Element zabrany.", ConsoleColor.Red );
                            }
                            DisplayMagazineState();
                            if ( magElemAmount <= 0 )
                            {
                                isConsThreadActive = false;
                                Console.WriteLine( "Wątek konsumenta został uśpiony" );
                            }
                            if ( !isProdThreadActive )
                            {
                                Console.WriteLine( "Wątek producenta jest wznawiany" );
                                isProdThreadActive = true;
                            }
                            Thread.Sleep( r.Next( maxConsTime ) );
                        }
                    }
                };

            prodThread = new Thread( prodAction );
            prodThread.IsBackground = true;
            prodThread.Start();

            consThread = new Thread( consAction );
            consThread.IsBackground = true;
            consThread.Start();

            Console.ReadLine();
            Console.WriteLine( "Koniec" );
            prodThread.Abort();
            consThread.Abort();
            DisplayMagazineState();
        }
    }

    [CInf(4, 5, "Nowy kod ProducentKonsument z użyciem Monitor")]
    class Section_4_5
    {
        static object magSyncObj = new object();
        static object prodSyncObj = new object();
        static object consSyncObj = new object();
        static Random r = new Random();

        static Thread prodThread = null;
        static Thread consThread = null;

        const int maxProdTime = 1000,
                    maxConsTime = 1000,
                    maxProdLaunchTime = 5000,
                    maxConsLaunchTime = 5000;

        static int magCapacity = 20;
        static int magElemAmount = 1;

        static void DisplayMagazineState()
        {
            Console.WriteLine( "Liczba elementów w magazynie: " + magElemAmount );
        }

        [FInf(4, 5, "Uruchamianie uproszczonego kodu" )]
        public static void Main_4_5()
        {
            ThreadStart prodAction = () =>
                {
                    Console.WriteLine( "Wątek producenta jest uruchamiany" );
                    while (true)
                    {
                        lock ( prodSyncObj )
                        {
                            magElemAmount++;
                            ConsoleEx.WriteLine( "Element dodany.", ConsoleColor.Green );
                        }
                        DisplayMagazineState();
                        if ( magElemAmount >= magCapacity )
                        {
                            Console.WriteLine( "Wątek producenta zostanie uśpiony" );
                            lock ( prodSyncObj ) Monitor.Wait( prodSyncObj );
                            Console.WriteLine( "Wątek producenta zostanie wzniowiony" );
                            Thread.Sleep( r.Next( maxProdLaunchTime ) );
                            Console.WriteLine( "Wątek producenta został wznowiony" );
                        }
                        lock ( consSyncObj ) Monitor.Pulse( consSyncObj );
                        Thread.Sleep( r.Next( maxProdTime ) );
                    }
                };

            ThreadStart consAction = () =>
                {
                    Console.WriteLine( "Wątek konsumenta jest uruchamiany" );
                    while(true)
                    {
                        lock(magSyncObj)
                        {
                            magElemAmount--;
                            ConsoleEx.WriteLine( "Element zabrany", ConsoleColor.Red );
                        }
                        DisplayMagazineState();
                        if (magElemAmount <=0)
                        {
                            Console.WriteLine( "Wątek konsumenta zostanie uśpiony" );
                            lock ( consSyncObj ) Monitor.Wait( consSyncObj );
                            Console.WriteLine( "Wątek konsumenta zostanie wznowiony" );
                            Thread.Sleep( r.Next( maxConsLaunchTime ) );
                            Console.WriteLine( "Wątek konsumenta został wznowiony" );
                        }
                        lock(prodSyncObj) Monitor.Pulse( prodSyncObj );
                        Thread.Sleep( r.Next( maxConsLaunchTime ) );
                    }
                };

            prodThread = new Thread( prodAction ) { IsBackground = true };
            prodThread.Start();

            consThread = new Thread( consAction ) { IsBackground = true };
            consThread.Start();

            Console.ReadLine();
            Console.WriteLine( "Koniec. " );
            DisplayMagazineState();
        }
    }
}
