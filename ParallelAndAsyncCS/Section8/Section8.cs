﻿using AJ.ConsoleExtension;
using AJ.Reflection;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelAndAsyncCS.Section8
{

    class Konto
    {
        private decimal saldo;
        private int id;

        public Konto( decimal saldoPoczatkowe, int id )
        {
            saldo = saldoPoczatkowe;
            this.id = id;
        }

        public void Wyplata( decimal kwota )
        {
            saldo -= kwota;
            //Console.WriteLine( "Nastąpiła wypłata z konta {0} kwoty {1}. Saldo po operacji {2}", id, kwota, saldo );
        }

        public void Wplata( decimal kwota )
        {
            saldo += kwota;
            //Console.WriteLine( "Nastąpiła wpłata na konto {0} kwoty {1}. Saldo po operacji {2}", id, kwota, saldo );
        }

        public static void Przelew_8_1( Konto kontoPlatnika, Konto kontoOdbiorcy, decimal kwota )
        {
            if ( kontoOdbiorcy == kontoPlatnika ) throw new ArgumentException( "Niemożliwe jest wykonanie przelewu na to samo konto" );

            Console.WriteLine( "Przygotowanie do przelewu z konta {0} na konto {1} kwoty {2}.", kontoPlatnika.id, kontoOdbiorcy.id, kwota );
            Console.WriteLine( "Salda przed przelewem: konto {0} - saldo {1}, konto {2} - saldo {3}", kontoPlatnika.id, kontoPlatnika.saldo, kontoOdbiorcy.id, kontoOdbiorcy.saldo );

            lock ( kontoPlatnika )
            {
                Console.WriteLine( "Dostep do konta płatnika {0} zarezerwowany", kontoPlatnika.id );
                Thread.Sleep( 1000 );
                lock ( kontoOdbiorcy )
                {
                    Console.WriteLine( "Dostep do konta odbiorcy {0} zarezerwowany", kontoOdbiorcy.id );
                    kontoPlatnika.Wyplata( kwota );
                    kontoOdbiorcy.Wplata( kwota );
                }
                Console.WriteLine( "Dostęp do konta odbiorcy {0} zwolniony", kontoOdbiorcy.id );
            }

            Console.WriteLine( "Dostęp do konta płatnika {0} zwolniony", kontoPlatnika.id );
        }
    }

    class PoleceniePrzelewu
    {
        public Konto KontoPlatnika;
        public Konto KontoOdbiorcy;
        public decimal Kwota;
    }

    [CInf( 8, 1, "Synchronizacja zadań" )]
    public class Section_8_1
    {
        [FInf( 8, 1, "Klasyczny przykład operacji bankowych - sekcje krytyczne" )]
        public void Main_8_1()
        {
            Konto k1 = new Konto( 100, 1 );
            Konto k2 = new Konto( 150, 2 );

            Action<object> transakcja = ( object parametr ) =>
            {
                PoleceniePrzelewu pp = parametr as PoleceniePrzelewu;
                if ( pp == null ) throw new ArgumentNullException( "Brak polecenia przelewu" );
                else Konto.Przelew_8_1( pp.KontoPlatnika, pp.KontoOdbiorcy, pp.Kwota );
            };

            Task.Factory.StartNew( transakcja, new PoleceniePrzelewu { KontoPlatnika = k1, KontoOdbiorcy = k2, Kwota = 50 } );
            Task.Factory.StartNew( transakcja, new PoleceniePrzelewu { KontoPlatnika = k1, KontoOdbiorcy = k2, Kwota = 10 } );

            Console.ReadLine(); // wątek główny czeka na dodatkowe wątki
        }
    }

    [CInf(8,2,"Komunikacja między zadaniami")]
    public class Section8_2
    {
        static object magSyncObj = new object();
        static object consSyncObj = new object();
        static object prodSyncObj = new object();
        static Random r = new Random();

        static Task prodTask = null;
        static Task consTask = null;

        const int maxProdTime = 1000;
        const int maxConsTime = 1000;
        const int maxProdLaunchTime = 5000;
        const int maxConsLaunchTime = 5000;

        static int magCapacity = 20;
        static int magElemAmount = 10;

        static void DisplayMagazineState()
        {
            Console.WriteLine( "Liczba elementów w magazynie: " + magElemAmount );
        }

        [FInf( 8, 2, "Komunikacja między zadaniami" )]
        public static void Main_8_2()
        {
            Action prodAction = () =>
            {
                Console.WriteLine( "Wątek producenta jest uruchamiany" );
                while ( true )
                {
                    lock ( prodSyncObj )
                    {
                        magElemAmount++;
                        ConsoleEx.WriteLine( "Element dodany.", ConsoleColor.Green );
                    }
                    DisplayMagazineState();
                    if ( magElemAmount >= magCapacity )
                    {
                        Console.WriteLine( "Wątek producenta zostanie uśpiony" );
                        lock ( prodSyncObj ) Monitor.Wait( prodSyncObj );
                        Console.WriteLine( "Wątek producenta zostanie wzniowiony" );
                        Thread.Sleep( r.Next( maxProdLaunchTime ) );
                        Console.WriteLine( "Wątek producenta został wznowiony" );
                    }
                    lock ( consSyncObj ) Monitor.Pulse( consSyncObj );
                    Thread.Sleep( r.Next( maxProdTime ) );
                }
            };

            Action consAction = () =>
            {
                Console.WriteLine( "Wątek konsumenta jest uruchamiany" );
                while ( true )
                {
                    lock ( magSyncObj )
                    {
                        magElemAmount--;
                        ConsoleEx.WriteLine( "Element zabrany", ConsoleColor.Red );
                    }
                    DisplayMagazineState();
                    if ( magElemAmount <= 0 )
                    {
                        Console.WriteLine( "Wątek konsumenta zostanie uśpiony" );
                        lock ( consSyncObj ) Monitor.Wait( consSyncObj );
                        Console.WriteLine( "Wątek konsumenta zostanie wznowiony" );
                        Thread.Sleep( r.Next( maxConsLaunchTime ) );
                        Console.WriteLine( "Wątek konsumenta został wznowiony" );
                    }
                    lock ( prodSyncObj ) Monitor.Pulse( prodSyncObj );
                    Thread.Sleep( r.Next( maxConsLaunchTime ) );
                }
            };

            prodTask = new Task( prodAction );
            prodTask.Start();

            consTask = new Task( consAction );
            consTask.Start();

            Console.ReadLine();
            Console.WriteLine( "Koniec. " );
            DisplayMagazineState();
        }
    }

    [CInf(8, "Synchronizacja zadań")]
    public class Section8
    {
        const int taskCount = 10;
        static Barrier btask = new Barrier( taskCount, ( Barrier _b ) =>
            {
                Console.WriteLine();
            } );

        const int threadCount = 10;
        static Barrier bthread = new Barrier( threadCount, ( Barrier _b ) =>
            {
                Console.WriteLine();
            } );

        [FInf(8, 2, "Użycie bariery")]
        public static void Main8_3()
        {
            Action taskMethod = () =>
                {
                    for ( int i = 0 ;i < 10 ;++i )
                    {
                        Console.Write( i.ToString() );
                        btask.SignalAndWait();
                    }
                };

            Task[] tasks = new Task[taskCount];
            for ( int i = 0 ;i < taskCount ;i++ )
            {
                tasks[i] = new Task( taskMethod );
                tasks[i].Start();
            }

            Console.ReadLine();
        }

        [FInf(8,3,"Bariera w przypadku puli wątków")]
        public static void Main8_4()
        {
            Action<object> threadMethod = ( object param ) =>
                {
                    for ( int i = 0 ;i < 10 ;i++ )
                    {
                        Console.Write( i.ToString() );
                        bthread.SignalAndWait();
                    }
                };

            for ( int i = 0 ;i < threadCount ;i++ )
            {
                ThreadPool.QueueUserWorkItem( new WaitCallback( threadMethod ) );
            }

            Console.ReadLine();
        }
    }
}
