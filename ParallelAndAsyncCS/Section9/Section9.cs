﻿using AJ.Reflection;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelAndAsyncCS.Section9
{
    [CInf(9, "Dane w programach równoległych")]
    class Section9
    {
        [FInf(9,1,"Podstawy korzsytania z kolekcji współbierznych")]
        public void Section9_1()
        {
            var cb  = new ConcurrentBag<int>();
            var l = new List<int>();

            Parallel.For( 0, 10000, ( i ) =>
                {
                    cb.Add( i );
                    l.Add( i );
                } );
        }

        [FInf(9,2,"Praca z klasami ConcurrentStack i ConcurrentQueue")]
        public void Section9_2()
        {
            // zapełnianie kolekcji
            int rozmiar = 10;
            var kolejka = new ConcurrentQueue<int>();
            var stos = new ConcurrentStack<int>();
            for( int i = 0; i< rozmiar; ++i)
            {
                kolejka.Enqueue(i);
                stos.Push( i );
            }

            // pobierane elementów
            int element;
            string s = "Elementy zdjęte z kolejki (" + kolejka.Count + " elementów):\n";

            for( int i = 0; i < rozmiar; ++i)
            {
                kolejka.TryDequeue( out element );
                s += element.ToString() + " ";
            }
            s += "\n\nElementy zdjęte ze stosu (" + stos.Count + " elementów):\n";
            for (int i = 0; i < rozmiar; ++i)
            {
                stos.TryPop( out element );
                s += element.ToString() + " ";
            }
        }

        [FInf(9,3,"Przykładowe operacje klasy BlockingCollection")]
        public void Section9_3()
        {
            var kolekcja = new BlockingCollection<int>( 3 );
            // można i tak
            //kolekcja = new BlockingCollection<int>(new  MyStack<int>() );

            Action producent = () =>
                {
                    for ( int i = 0 ;i < 5 ;i++ )
                    {
                        kolekcja.Add( i );
                        Console.WriteLine( "Dodano element {0}", i );
                    }
                    Thread.Sleep( 5000 );
                    kolekcja.CompleteAdding();
                };
            
            Action konsument = () =>
                {
                    Thread.Sleep(5000);
                    foreach(int i in kolekcja.GetConsumingEnumerable())
                    {
                        Console.WriteLine("Pobrano element {0}", i);
                    }
                };

            Parallel.Invoke(
                producent,
                konsument);
        }

        [CInf(9, 4, "Prosta implementacja interfejsu IProducerConsumerCollection<>")]
        public class MyStack<T> : IProducerConsumerCollection<T>
        {
            private object obj = new object();
            private Stack<T> stos = null;

            public MyStack()
            {
                stos = new Stack<T>();
            }

            public MyStack(IEnumerable<T> collection)
            {
                stos = new Stack<T>( collection );
            }

            public void Push(T element)
            {
                lock ( obj ) stos.Push( element );
            }

            public bool TryPop( out T element)
            {
                bool result = true;
                lock(obj)
                {
                    if ( stos.Count == 0 ) { element = default( T ); result = false; }
                    else element = stos.Pop();
                }
                return result;
            }

            public void CopyTo( T[] array, int index )
            {
                lock ( obj ) stos.CopyTo( array, index );
            }

            public T[] ToArray()
            {
                T[] result = null;
                lock ( obj ) result = stos.ToArray();
                return result;
            }

            public bool TryAdd( T item )
            {
                Push( item );
                return true;
            }

            public bool TryTake( out T item )
            {
                return TryPop( out item );
            }

            public IEnumerator<T> GetEnumerator()
            {
                Stack<T> stosCopy = null;
                lock ( obj ) stosCopy = new Stack<T>( stos );
                return stosCopy.GetEnumerator();
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return ((IEnumerable<T>)this).GetEnumerator();
            }

            public void CopyTo( Array array, int index )
            {
                lock ( obj ) ((ICollection)stos).CopyTo( array, index );
            }

            public int Count
            {
                get { return stos.Count; }
            }

            public bool IsSynchronized
            {
                get { return true; }
            }

            public object SyncRoot
            {
                get { return obj; }
            }
        }

        public void Section9_3_1()
        {
            List<string> lista = new List<string>();

            lista.AddRange(new string[]{"Ala","ma","kota"});

            string wynik = lista.Aggregate(
                "",
                ( suma, elem ) => string.Concat( suma, elem ),
                ( suma ) => suma );

            Console.WriteLine( wynik );
        }

        [FInf(9,5,"Równoległe obliczenie odległości w przestrzeni euklidesowej")]
        public void Section9_4()
        {
            int[] coords = { 3, 4 };

            double dist = coords.AsParallel().Aggregate(
                0,                          // wartość początkowa
                ( s, i ) => s + i * i,          // redukcja pośrednia
                ( s1, s2 ) => s1 + s2,        // redukcja końcowa
                ( r ) => Math.Sqrt( (double)r ) // konwersja
                );
        }

        [FInf(9,6, "Obliczanie liczby Pi przy użyciu agregacji")]
        public void Section9_6()
        {
            Console.WriteLine( "Ścisła wartość Pi: {0}", Math.PI );

            const int range = 10000000;
            int time;

            Func<int, double> sign = ( i ) =>
                {
                    if ( i % 2 == 1 )
                        return -1;
                    return 1;
                };
            Func<int, double> nparz = ( i ) =>
                {
                    return (double)i * 2 + 1;
                };
            Func<int, double> ciag = ( i ) =>
                {
                    return sign( i ) / nparz( i );
                };

            IEnumerable<double> query =
                from i in new ConcurrentBag<int>( Enumerable.Range( 0, range ) )
                select ciag( i );

            time = Environment.TickCount;
            var result1 = 4 * query.Sum();
            time = Environment.TickCount - time;
            Console.WriteLine( "Wynik serw. Sum : {0} (czas:{1}ms)", result1, time );

            time = Environment.TickCount;
            var result2 = 4 * query.AsParallel().Sum();
            time = Environment.TickCount - time;
            Console.WriteLine( "Wyrnik równ. Sum : {0} (czas:{1}ms)", result2, time );

            time = Environment.TickCount;
            var result3 = 4 * new ConcurrentBag<int>( Enumerable.Range( 0, range ) ).
                Aggregate<int, double, double>(
                0.0,
                ( sum, i ) => sum + ciag( i ),
                ( sum ) => sum );
            time = Environment.TickCount - time;
            Console.WriteLine( "Wyrnik sekw. Aggregate : {0} (czas:{1}ms)", result3, time );

            time = Environment.TickCount;
            var result4 = 4 * new ConcurrentBag<int>( Enumerable.Range( 0, range ) ).
                AsParallel().Aggregate<int, double, double>(
                0.0,
                ( sum, i ) => sum + ciag( i ),
                (sum1, sum2) => sum1 + sum2,
                ( sum ) => sum );
            time = Environment.TickCount - time;
            Console.WriteLine( "Wyrnik równ. Aggregate : {0} (czas:{1}ms)", result4, time );
        }

        static string[] list = {"Toruń", "Olsztyn", "Katowice", "Kraków", "Warszawa", "Bydgoszcz",
                         "Gdańsk", "Szczecin", "Wrocław", "Poznań", "Włocławek", "Inowrocław", "Olecko"};

        [FInf(9,7,"Zapytanie LINQ")]
        public void Listing9_7()
        {
            var query = from m in list
                        where m.StartsWith( "W" )
                        orderby m.ToUpper()
                        select m;
        }

        [FInf(9,8,"Zapytanie PLINQ")]
        public void Listing9_8()
        {
            var query = from m in list.AsParallel()
                        where m.StartsWith( "W" )
                        orderby m.ToUpper()
                        select m;
        }

        [FInf(9,9,"Zapytanie jawne PLINQ")]
        public void Listing9_9()
        {
            var query = list.AsParallel()
                        .Select ( m => { return m; } )
                        .Where  ( m => { return m.StartsWith("W");})
                        .OrderBy( m => { return m.ToUpper(); } );
        }

        [FInf(9,10,"Zapytanie, którego nie da się efektywnie zrównoleglić")]
        public void Listing9_10()
        {
            var query = from m in list.AsParallel()
                        where (m.ToUpper().StartsWith( "W" ) == true)
                        orderby m.ToUpper()
                        select m;
        }

        [FInf(9,11,"Zapytanie podatne na zrównoleglenie")]
        public void Listing9_11()
        {
            Func<string, bool> funkcja = ( s ) =>
                {
                    Thread.SpinWait( 10000000 );
                    if ( s.ToUpper().StartsWith( "W" ) == true )
                    {
                        return true;
                    }
                    return false;
                };

            var query = from m in list.AsParallel()
                        where funkcja( m )
                        orderby m.ToUpper()
                        select m;
        }

        [FInf(9,12,"Funkcja najpierw wykonywana sekwencyjnie, potem równolegle")]
        public void Listing9_12()
        {
            var query = list.Select( m => { return m;})// niebezpieczna funkcja
                        .AsParallel()
                        .Where( m => { return (int)m[0] % 2 == 0; } )
                        .OrderBy(m=>{return m;});
        }

        [FInf(9,13, "Przerywanie zapytania PLINQ")]
        public void Listing9_13()
        {
            IEnumerable<int> lista = Enumerable.Range( 0, 100 );
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            //Listing 9.14
            Func<int, bool> funkcja = (i) =>
                {
                    //Thread.SpinWait(10000000);
                    if(i%2==0)
                        return true;
                    return false;
                };

            var query = lista.AsParallel().WithCancellation( ct )
                        .Select( m => { return m; } )
                        .Where( m => { return funkcja( m ); } )
                        .OrderBy( m => { return m; } );
            cts.Cancel();
            try
            {
                foreach ( var i in query )
                    Console.WriteLine( i );
            }
            catch
            {
                Console.WriteLine( "Wystąpiło przerwanie" );
            }
        }

        [FInf(9,15, "Obsługa przerwania zapytanie w trakcie jego wykonywania")]
        public void Listing9_15()
        {
            IEnumerable<int> lista = Enumerable.Range( 0, 100 );
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            //Listing 9.14
            Func<int, bool> funkcja = (i) =>
                {
                    //Thread.SpinWait(10000000);
                    if(i%2==0)
                        return true;
                    return false;
                };

            var query = lista.AsParallel().WithCancellation( ct )
                        .Select( m => { return m; } )
                        .Where( m => { return funkcja( m ); } )
                        .OrderBy( m => { return m; } );
            cts.Cancel(); 

            foreach( var i in query)
            {
                try
                {
                    Console.WriteLine( i );
                    ct.ThrowIfCancellationRequested();
                }
                catch
                {
                    Console.WriteLine( "Wystąpiło przerwanie" );
                    break;
                }
            }
        }

        [FInf(9, 16, "Pełna obsługa wyjątków PLINQ")]
        public void Listing9_16()
        {
            IEnumerable<int> lista = Enumerable.Range( 0, 100 );
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            var query = Enumerable.Range( 0, 100 ).AsParallel().WithCancellation( ct )
                        .Select( m => { return m; } )
                        .Where( m => { return true; } )
                        .OrderBy( m => { return m; } );

            try
            {
                foreach(var i in query)
                {
                    if(ct.IsCancellationRequested)
                    {
                        throw new AggregateException(
                            new Exception( "Błąd!" ),
                            new OperationCanceledException() );
                    }
                    Console.WriteLine( i );
                }
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine( "Wystąpiło przerwanie zapytania" );
            }
            catch (AggregateException ae)
            {
                foreach( Exception e in ae.InnerExceptions)
                {
                    if ( e.GetType() == typeof( OperationCanceledException ) )
                        Console.WriteLine( "Wystąpiło przerwanie" );
                    else
                        Console.WriteLine( "Inny błąd: {0}", e.Message );
                }
            }
        }

        [FInf(9, 17, "Przykład wykorzystania metody ForAll - wypisanie liczb parzystych")]
        public void Listing9_17()
        {
            Enumerable.Range( 0, 100 ).AsParallel()
                .Where( i => { return i % 2 == 0; } )
                .ForAll( i => { Console.WriteLine( i ); } );
        }
    }
}
