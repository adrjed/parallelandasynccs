﻿using AJ.ConsoleExtension;
using AJ.Reflection;
using System;
using System.Threading;
using System.Threading.Tasks;
using msgBox = System.Windows.MessageBox;


namespace ParallelAndAsyncCS.Section1
{
    class S1_1_Overview
    {
        // Zadanie które asynchronicznie wykonuje Action delegata, który nic nie zwraca
        //Task task;
        // Delegat, w którym przekazujemy funkcje do wykonania asynchronicznie, które
        // mogą przyjmować argumenty, ale nic nie zwracają
        //Action action;

        // Zadanie, które asynchronicznie wykonuje Func delegata, który zwraca określony
        // generycznie typ
        //Task<int> gtask;
        // Delegat, w którym przekazujemy funkcję, która może przyjmować parametry, a także
        // zwraca określony typ w zadaniu ( gtask.Result zwróci np. typ int )
        //Func<int> func;
    }

    [CInf(1, 1, "Programowanie asynchroniczne. Operator await i modyfikator async - C# 5.0 i .Net 4.5")]
    public class Section_1_1
    {
        /// <summary>Listing 1.1 - Synchroniczne wykonywanie kodu zawartego w akcji</summary>
        [FInf(1, 1, "Synchroniczne wykonywanie kodu zawartego w akcji")]
        public void button_Click_1_1(object sender, EventArgs e)
        {
            Func<object, long> akcja = ( object argument ) =>
                {
                    msgBox.Show( "Akcja: Początek, argument: " + argument.ToString() );
                    Thread.Sleep( 1000 ); //opóźnienie
                    msgBox.Show( "Akcja: Koniec" );
                    return DateTime.Now.Ticks;
                };

            msgBox.Show( "button_Click: Początek" );
            msgBox.Show( "Wynik: " + akcja( "synchronicznie" ) );
            msgBox.Show( "button_Click_1_1 : Koniec" );
        }

        /// <summary>Listing 1.2 - Użycie zadania do asynchronicznego wykonywania kodu</summary>
        [FInf(1, 2, "Użycie zadanie do asynchronicznego wykonywania kodu")]
        public void button_Click_1_2(object sender, EventArgs e)
        {
            // Func delegate - przyjmuje wiele agrumentów i zwraca określony typ
            // Action delegate - przyjmuje wiele argumentów, nic nie zwraca
            Func<object, long> akcja = ( object argument ) =>
            {
                msgBox.Show( "Akcja: Początek, argument: " + argument.ToString() );
                Thread.Sleep( 1000 ); //opóźnienie
                msgBox.Show( "Akcja: Koniec" );
                return DateTime.Now.Ticks;
            };

            // generyczny Task - we właściwości Result zwróci określony typ
            Task<long> zadanie = new Task<long>( akcja, "zadanie" );
            zadanie.Start();

            msgBox.Show( "Akcja została uruchomiona" );
            if ( zadanie.Status != TaskStatus.Running && zadanie.Status != TaskStatus.RanToCompletion )
                msgBox.Show( "Zadanie nie zostało uruchomione" );
            else
                msgBox.Show( "Wynik: " + zadanie.Result );
            msgBox.Show( "button_Click_1_2 : Koniec" );
        }

        /// <summary>Listing 1.3 - Wzór metody wykonującej jakąś czynność asynchronicznie</summary>
        /// <param name="argument">Coś do wyświetlenia w MessageBox</param>
        /// <returns>Obiekt Task wykonywanego zadania</returns>
        [FInf(1, 3, 0, "Wzór metody wykonującej jakaś czynnośc asynchronicznie")]
        public Task<long> DoSomethingAsync(object argument)
        {
            Func<object, long> akcja = ( object _argument ) =>
                {
                    msgBox.Show( "Akcja: Początek, argument: " + _argument.ToString() );
                    Thread.Sleep( 1000 ); //opóźnienie
                    msgBox.Show( "Akcja: Koniec" );
                    return DateTime.Now.Ticks;
                };

            Task<long> zadanie = new Task<long>( akcja, argument );
            zadanie.Start();
            // Zwraca referencję do zadania, które wykonuje w tym momencie działa
            return zadanie;
        }

        /// <summary>Listing 1.3 - Wywoływanie metody wykonującej jakąś czynność asynchronicznie</summary>
        [FInf(1, 3, 1, "Wywoływanie metody wykonującej jakaś czynnośc asynchroniczne")]
        public void button_Click_1_3(object sender, EventArgs e)
        {
            msgBox.Show( "button_Click_1_3 : Początek" );
            // Przypisujemy obiektowi zadanie funkcję, która w tym momencie wykonana zwróci
            // Task<long> DoSomethingAsync w której wykonywane są już jakieś akcje 
            Task<long> zadanie = this.DoSomethingAsync( "zadanie-metoda" );
            msgBox.Show( "Akcja została uruchomiona" );
            // Sprawdzany status wykonywania zwróconego zadania
            if ( zadanie.Status != TaskStatus.Running && zadanie.Status != TaskStatus.RanToCompletion )
                msgBox.Show( "Zadanie nie zostało uruchomione" );
            else
                msgBox.Show( "Wynik : " + zadanie.Result );
            msgBox.Show( "button_Click_1_3 : Koniec" );
        }

        /// <summary>Listing 1.4 - Przykład użycia modyfikatora async i await</summary>
        [FInf(1, 4, "Przykład użycia modyfikatora async i await")]
        public async void button_Click_1_4(object sender, EventArgs e)
            // async - oznaczamy funkcję, która będzie w swym ciele uruchamiała asynchroniczne zadanie,
            // która zwróci swój wynik za pomocą Task<T>.Result
        {
            msgBox.Show( "button_Click_1_4 : Początek" );
            Task<long> zadanie = this.DoSomethingAsync( "async/await" );
            msgBox.Show( "Akcja została uruchomiona" );
            long wynik = await zadanie; // operator await zwraca parametr użyty w klasie parametrycznej Task<>
            // To miejsce w kodzie będzie miejscem zatrzymania się funkcji (ponieważ chcemy użyć rezultatu
            // pracy zadania Task, czyki wynik), wyjścia z ciała funkcji i wykonywania kolejnych instrukcji
            // Dopóki Task zadanie nie ogłosi, że dostępny jest wynik operacji wykonywany w innym wątku(Tasku).
            // Wtedy funkcji button_Click_1_4 dokończy swoje działanie
            msgBox.Show( "Wynik : " + wynik );
            msgBox.Show( "button_Click_1_4 : Koniec" );
        }

        // Kod na szybko
        private async Task<int> AsyncIntReturn(int arg)
        {
            Func<int> intFunc = new Func<int>(()=>
            {
                Thread.Sleep(1000);
                return arg*2;
            });

            Task<int> intTask = new Task<int>(intFunc);
            return await intTask;           
        }

        /// <summary>Listing 1.5 1 - Działanie modyfikatora async</summary>
        [FInf(1, 5, 1, "Działanie modyfikatora async 1")]
        private async void button_Click_1_5_1( object sender, EventArgs e )
        {
            msgBox.Show( "button_Click_1_5_1 : Początek" );
            long wynik = await this.DoSomethingAsync( "async/await" );
            msgBox.Show( "Wynik : " + wynik.ToString() );
            msgBox.Show( "button_Click_1_5_1 : Koniec" );
        }

        /// <summary>Listing 1.5 2 - Działanie modyfikatora async</summary>
        [FInf(1, 5, 2, "Działanie modyfikatora async 2")]
        //public void button_Click_1_5_2(object sender, EventArgs e)
        public void button_Click_1_5_2()
        {
            msgBox.Show( "button_Click_1_5_2 : Początek" );
            button_Click_1_5_1( null, null );
            msgBox.Show( "button_Click_1_5_2 : Koniec" );
        }

        /// <summary>Listing 1.6 - Metoda async zwracająca zadanie</summary>
        [FInf(1, 6, 0, "Metoda async zwracająca zadanie")]
        public async Task DoSomethingMoreAsync_1_6()
        {
            msgBox.Show( "DoSomethingMoreAsync : Początek" );
            long wynik = await DoSomethingAsync( "async/await" );
            msgBox.Show( "DoSomethingMoreAsync : Wynik" + wynik.ToString() );
            msgBox.Show( "DoSomethingMoreAsync : Koniec" );
            return;
        }

        /// <summary>Listing 1.6 - Zdarzenie kliknięcia wywołujące zadanie asynchroniczne</summary>
        [FInf(1, 6, 1, "Zdarzenie kliknięcia wywołujące zadanie asynchronicznie")]
        public async void button_Click_1_6(object sender, EventArgs e)
        {
            msgBox.Show( "button_Click_1_6 : Początek" );
            await DoSomethingMoreAsync_1_6();
            // Funkcja nic nie zróci, ale w tym miejscu wyjdziemy z ciała button_Click()
            // do czasu wykonania DoSomethingMoreAsync()
            msgBox.Show( "button_Click_1_6 : Koniec" );
        }

        /// <summary>Listing 1.7 - Metoda async zwracająca wartość long</summary>
        [FInf(1, 7, 0, "Metoda async zwracająca wartość long")]
        public async Task<long> DoSomethingMoreAsync_1_7()
        {
            msgBox.Show( "DoSomethingMoreAsync : Początek" );
            long wynik = await DoSomethingAsync( "async/await" );
            msgBox.Show( "DoSomethingMoreAsync : Wynik" + wynik.ToString() );
            msgBox.Show( "DoSomethingMoreAsync : Koniec" );
            return wynik;
        }

        /// <summary>Listing 1.7 - Zdarzenie przycisku do wywołania metody async zwracającej wartość long</summary>
        [FInf(1, 7, 1, "Zdarzenie przycisku do wywołania metody async zwracającej wartość long")]
        public async void button_Click_1_7(object sender, EventArgs e)
        {
            msgBox.Show( "button_Click_1_7 : Początek" );
            msgBox.Show( "button_Click_1_7 : Wynik: " + await DoSomethingMoreAsync_1_7() );
            msgBox.Show( "button_Click_1_7 : Koniec" );
        }

        /// <summary>Listing 1.8 - Obsługa wyjątków zgłaszanych przez metody async</summary>
        [FInf(1, 8, "Obsługa wyjątków zgłaszanych przez metody async")]
        public async void button_Click_1_8(object sender, EventArgs e)
        {
            msgBox.Show( "button_Click_1_8 : Początek" );
            try
            {
                msgBox.Show( "button_Click_1_8 : Wynik: " + await DoSomethingMoreAsync_1_7() );
            }
            catch (Exception exc)
            {
                msgBox.Show( "button_Click_1_8 : Błąd!\n" + exc.Message );
            }
            msgBox.Show( "button_Click_1_8 : Koniec" );
        }
    }
    
    [CInf(1, 2, "Klasa Parallel z biblioteki TPL - .Net 4.0")]
    public class Section_1_2
    {

        /// <summary>Listing 1.9 - Metoda zajmująca procesor</summary>
        [FInf(1, 9, "Metoda zajmująca procesor")]
        public static double Obliczenia_1_9(double arg)
        {
            for ( int i=0 ;i < 10 ;++i )
                arg = Math.Asin( Math.Sin( arg ) );

            return arg;
        }

        /// <summary>Listing 1.10 - Obliczenia sekwencyjne</summary>
        [FInf(1, 10, "Obliczenia sekwencyjne")]
        public static void Main_1_10(/*string[] args*/)
        {
            // przygotowania
            int rozmiar = 10000;
            Random r = new Random(DateTime.Now.Millisecond);
            double[] tablica = new double[rozmiar];

            for(int i=0; i<tablica.Length; ++i)
                tablica[i] = r.NextDouble();

            //obliczenia sekwencyjne
            int powtorzen = 100;
            double[] wyniki = new double[tablica.Length];
            int start = System.Environment.TickCount;

            for ( int powtorzenia = 0 ;powtorzenia < powtorzen ;++powtorzenia )
            {
                for ( int i = 0 ;i < tablica.Length ;++i )
                {
                    wyniki[i] = Obliczenia_1_9( tablica[i] );
                }
            }

            int stop = System.Environment.TickCount;

            msgBox.Show( "Obliczenia sekwencyjne trwały " + (stop - start).ToString() + " ms." );
        
            // prezentacja wyników
            string s = "Wyniki: \n";
            for ( int i = 0 ;i < tablica.Length ;++i )
                s += i + ". " + tablica[i] + " ?= " + wyniki[i] + "\n";
            Console.WriteLine( s );
 
        }

        /// <summary>Listing 1.11 - Przykład zrównoleglonej pętli for</summary>
        [FInf(1, 11, "Przykład zrównoleglonej pętli for")]
        public static void Main_1_11(/* string[] args */)
        {
            // przygotowania
            int rozmiar = 10000;
            Random r = new Random( DateTime.Now.Millisecond );
            double[] tablica = new double[rozmiar];

            for ( int i = 0 ;i < tablica.Length ;++i )
                tablica[i] = r.NextDouble();

            //obliczenia sekwencyjne
            int powtorzen = 100;
            double[] wyniki = new double[tablica.Length];
            int start = System.Environment.TickCount;

            for ( int powtorzenia = 0 ;powtorzenia < powtorzen ;++powtorzenia )
            {
                Parallel.For( 0, tablica.Length, i => { wyniki[i] = Obliczenia_1_9( tablica[i] ); } );
            }

            int stop = System.Environment.TickCount;

            msgBox.Show( "Obliczenia równoległe trwały " + (stop - start).ToString() + " ms." );

            // prezentacja wyników
            string s = "Wyniki: \n";
            var cc = ConsoleColor.Gray;
            for ( int i = 0 ;i < tablica.Length ;++i )
            {
                //s += i + ". " + tablica[i] + " ?= " + wyniki[i] + "\n";
                s = i + ". " + tablica[i] + " ?= " + wyniki[i] + "\n";
                cc = cc == ConsoleColor.Gray ? ConsoleColor.White : ConsoleColor.Gray;
                ConsoleEx.Write( s ,cc);
            }

        }

        /// <summary>Listing 1.12 - przerywanie pętli równoległej</summary>
        [FInf(1, 12, "Przerywanie pętli równologłej")]
        public static void przerywaniePetli_1_12()
        {
            Random r = new Random();
            long suma = 0;
            long licznik = 0;
            string s = "";

            // iteracje zostaną wykonane tylko dla liczb parzystych
            // pętla zostanie przerwana wcześniej, jeśli wylosowana liczba jest większa od 90
            Parallel.For(
                0,
                10000,
                ( int i, ParallelLoopState stanPetli ) =>
                {
                    int liczba = r.Next( 7 ); //losowanie liczby oczek na kostce
                    if(liczba == 0)
                    {
                        s += "0 (Stop);";
                        stanPetli.Stop();
                    }
                    if ( stanPetli.IsStopped ) return;
                    if(liczba %2 == 0)
                    {
                        s += liczba.ToString() + "; ";
                        Obliczenia_1_9( liczba );
                        suma += liczba;
                        licznik += 1;
                    }
                    else
                    {
                        s += liczba.ToString() + "; ";
                    }
                } );

            Console.WriteLine(
                "Wylosowane liczby: " + s + "\n" +
                "Liczba pasujących liczb: " + licznik + "\n" +
                "Suma: " + suma + "\n" +
                "Średnia: " + (suma / (double)licznik).ToString() );
        }


    }
}
