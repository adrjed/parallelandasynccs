﻿using AJ.Reflection;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelAndAsyncCS.Section7
{
    [CInf(7, "Klasa Parallel")]
    class Section7
    {
        [FInf( 7, 2, "Przykład wywołania równoległej pętli For" )]
        public void Listing7_2()
        {
            Parallel.For( 0, 20, ( i ) => { Console.WriteLine( "Iteracja nr {0}, zadanie nr {1}", i, Task.CurrentId ); } );            
        }

        [FInf( 7, 3, "Sprawdzanie a pomocą For, czy liczba jest liczbą pierwszą" )]
        public void Listing7_3()
        {
            Console.Write( "Sprawdź, czy liczba jest pierwsza: " );
            int n = Int32.Parse( Console.ReadLine() );

            bool isPrime = true;
            Parallel.For( 2, (int)Math.Sqrt( n ) + 1, ( i ) =>
            {
                if ( n % (int)i == 0 )
                {
                    Console.WriteLine( "{0} dzieli się przez {1}", n, i );
                    isPrime = false;
                }
            } );

            if ( isPrime )
            {
                Console.WriteLine( "Liczba {0} jest liczbą pierwszą", n );
            }
        }

        [FInf( 7, 4, "Sprawdzanie za pomocą ForEach, czy liczba jest liczbą pierwszą" )]
        public void Listing7_4()
        {
            Console.WriteLine( "Sprawdź, czy liczba pierwsza: " );
            int n = Int32.Parse( Console.ReadLine() );

            bool isPrime = true;
            Parallel.ForEach<int>( Enumerable.Range( 2, (int)Math.Sqrt( n ) - 1 ), ( i ) =>
                {
                    if ( n % (int)i == 0 )
                    {
                        Console.WriteLine( "{0} dzieli się przez {1}", n, i );
                        isPrime = false;
                    }
                } );
            if ( isPrime )
            {
                Console.WriteLine( "Liczba {0} jest liczbą pierwszą", n );
            }


        }

        [FInf( 7, 5, "Przeszukiwanie drzewa binarnego z użyciem Parallel.Invoke" )]
        public void Listing7_5()
        {
            TreeWalk.Main7_5();
        }

        //Listing 7.5
        public static class TreeWalk
        {
            public static void Main7_5()
            {
                Tree<string> tree = new Tree<string>();

                tree.Data = "Darth Vader";
                (tree.Left = new Tree<string>()).Data = "Luke Skywalker";
                (tree.Right = new Tree<string>()).Data = "Princess Leia";

                Action<string> myAction = ( x ) => { Console.WriteLine( "{0} ({1})", x, Task.CurrentId ); };

                DoTree( tree, myAction );
            }

            public class Tree<T>
            {
                public Tree<T> Left;
                public Tree<T> Right;
                public T Data;
            }

            public static void DoTree<T>( Tree<T> tree, Action<T> action )
            {
                if ( tree == null ) return;
                Parallel.Invoke(
                    () => action( tree.Data ),
                    () => DoTree( tree.Left, action ),
                    () => DoTree( tree.Right, action ) );
            }
        }

        [FInf( 7, 6, "Wyszukiwanie liczby pierwszej przerywane po odnalezieniu pierwszego dzielnika" )]
        public void Listing7_6()
        {
            Console.Write( "Sprawdź, czy liczba pierwsza: " );
            int n = Int32.Parse( Console.ReadLine() );

            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            ParallelOptions po = new ParallelOptions();
            po.CancellationToken = ct;

            Console.WriteLine( "sprawdzam od 2 do {0}", (int)Math.Sqrt( n ) );

            try
            {
                Parallel.For( 2, (int)Math.Sqrt( n ) + 1, po, ( i ) =>
                    {
                        if ( n % (int)i == 0 )
                        {
                            cts.Cancel();
                        }
                        ct.ThrowIfCancellationRequested();
                    } );

                Console.WriteLine( "Liczba {0} jest liczbą pierwszą", n );
            }
            catch ( OperationCanceledException )
            {
                Console.WriteLine( "Liczba {0} nie jest liczbą pierwszą", n );
            }
        }

        [FInf( 7, 7, "Wyszukiwanie liczby pierwszej przy użyciu Parallel.ForEach" )]
        public void Listing7_7()
        {
            Console.Write( "Sprawdź, czy liczba pierwsza: " );
            int n = Int32.Parse( Console.ReadLine() );

            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            ParallelOptions po = new ParallelOptions();
            po.CancellationToken = ct;

            Console.WriteLine( "sprawdzam od 2 do {0}", (int)Math.Sqrt( n ) );

            try
            {
                Parallel.ForEach<int>( Enumerable.Range( 2, (int)Math.Sqrt( n ) - 1 ), po, ( i ) =>
                {
                    if ( n % (int)i == 0 )
                    {
                        cts.Cancel();
                    }
                    ct.ThrowIfCancellationRequested();
                } );

                Console.WriteLine( "Liczba {0} jest liczbą pierwszą", n );
            }
            catch ( OperationCanceledException )
            {
                Console.WriteLine( "Liczba {0} nie jest liczbą pierwszą", n );
            }
        }

        [FInf( 7, 8, "Przerywanie zadania za pomocą ParallelLoopState.Stop")]
        public void Listing7_8()
        {
            Console.Write( "Sprawdź, czy liczba pierwsza: " );
            int n = Int32.Parse( Console.ReadLine() );

            if (Parallel.For(2, (int)Math.Sqrt(n), (i, loopState) =>
                {
                    if (n % (int)i == 0)
                    {
                        Console.WriteLine("{0} dzieli się przez {1}", n, i);
                        loopState.Stop();
                    }
                }).IsCompleted)
            {
                Console.WriteLine( "Liczba {0} jest liczbą pierwszą", n );
            }
        }

        // Obliczanie Pi metodą Monte-Carlo

        [FInf( 7, 9, "Wersja sekwencyjna programu")]
        public void ObliczPiSekwencyjnie7_9()
        {
            Stopwatch sw = new Stopwatch();

            Console.Write( "Podaj liczbę iteracji: " );
            long n = long.Parse(Console.ReadLine());
            Random r = new Random();
            double x, y;
            long k = 0;

            sw.Start();
            for( long i = 0; i < n; i++)
            {
                x = r.NextDouble();
                y = r.NextDouble();
                if ( x * x + y * y < 1 ) k++;
            }
            sw.Stop();

            Console.WriteLine( "Wynik obliczeń: {0} - {1} ms", 4.0 * k / n, sw.ElapsedMilliseconds );
            //return 4.0 * k/n

        }


        [FInf( 7, 10, "Wersja równoległa algorytmu obliczającego liczbę Pi - bez synchronizacji")]
        public void ObliczPiRownolegle7_10()
        {
            Stopwatch sw = new Stopwatch();
            Console.Write( "Podaj liczbę iteracji: " );
            long n = long.Parse( Console.ReadLine() );
            Random r = new Random();
            double x, y;
            long k = 0;

            sw.Start();
            Parallel.For( 0, n, ( i ) =>
            {
                x = r.NextDouble();
                y = r.NextDouble();
                if ( x * x + y * y < 1 ) k++;
            } );
            sw.Stop();

            Console.WriteLine( "Wynik obliczeń: {0} - {1} ms", 4.0 * k / n, sw.ElapsedMilliseconds );
        }

        [FInf( 7, 11, "Wprowadzenie synchronizacji z użyciem operatora lock w mało wydajny sposób")]
        public void ObliczPiRownolegle7_11()
        {
            Stopwatch sw = new Stopwatch();
            Console.Write( "Podaj liczbę iteracji: " );
            long n = long.Parse( Console.ReadLine() );
            Random r = new Random();
            double x, y;
            long k = 0;
            object sync = new object();

            sw.Start();
            Parallel.For( 0, n, ( i ) =>
                {
                    lock ( r ) { x = r.NextDouble(); y = r.NextDouble(); }
                    if ( x * x + y * y < 1 ) lock ( sync ) { k++; }
                } );
            sw.Stop();

            Console.WriteLine( "Wynik obliczeń: {0} - {1} ms", 4.0 * k / n, sw.ElapsedMilliseconds );
        }

        [FInf(7, 12, "Synchronizacja zmiennej k powinna dotyczyć wątków, a nie zadań")]
        public void ObliczPiRownolegle7_12()
        {
            Stopwatch sw = new Stopwatch();
            Console.Write( "Podaj liczbę iteracji: " );
            long n = long.Parse( Console.ReadLine() );
            Random r = new Random();
            double x, y;
            long k = 0;

            sw.Start();
            Parallel.For( 0, n, () => 0, ( i, loopState, partialSum ) =>
            {
                lock ( r ) { x = r.NextDouble(); y = r.NextDouble(); }
                if ( x * x + y * y < 1 ) partialSum++;
                return partialSum;
            },
            ( partialSum ) => { Interlocked.Add( ref k, partialSum ); } );
            sw.Stop();

            Console.WriteLine( "Wynik: {0} - {1} ms", 4.0 * k / n , sw.ElapsedMilliseconds);
            //return 4.0 * k / n;
        }

        public static class RandomThreadSafe
        {
            private static Random _global = new Random();
            [ThreadStatic]
            private static Random _local;

            public static double NextDouble()
            {
                if(_local == null)
                {
                    int seed;
                    lock ( _global ) seed = _global.Next();
                    _local = new Random( seed );
                }
                return _local.NextDouble();
            }
        }

        [FInf(7,13,"Wielowątkowy generator liczb pseudolosowych oraz korzystająca z niego klasa RandomThreadSafe")]
        public void ObliczPiRownolegle7_13()
        {
            var sw = new Stopwatch();
            Console.Write("Podaj liczbę iteracji: ");
            long n = long.Parse( Console.ReadLine() );
            double x, y;
            long k = 0;

            sw.Start();
            Parallel.For( 0, n, () => 0, ( i, loopState, partialSum ) =>
            {
                x = RandomThreadSafe.NextDouble();
                y = RandomThreadSafe.NextDouble();

                if ( x * x + y * y < 1 )
                {
                    partialSum++;
                }
                return partialSum;
            },
            ( partialSum ) =>
            {
                Interlocked.Add( ref k, partialSum );
            } );
            sw.Stop();

            Console.WriteLine( "Wynik: {0} - {1} ms", 4.0 * k / n, sw.ElapsedMilliseconds );
        }

        [FInf( 7, 14, "Obliczanie Pi z wykorzystaniem klasy Partitioner - dopiero szybsze od sekwencyjnego" )]
        public void ObliczPiRownolegle7_14()
        {
            var sw = new Stopwatch();
            Console.Write( "Podaj liczbę iteracji: " );
            long n = long.Parse( Console.ReadLine() );
            long k = 0;

            sw.Start();
            Parallel.ForEach( Partitioner.Create( 0, n ), () => 0, ( przedzial, loopState, partialSum ) =>
                {
                    Random r = new Random( Task.CurrentId.Value + System.Environment.TickCount );
                    double x, y;

                    for ( long i = przedzial.Item1 ;i < przedzial.Item2 ;i++ )
                    {
                        x = r.NextDouble();
                        y = r.NextDouble();
                        if ( x * x + y * y < 1 ) partialSum++;
                    }
                    return partialSum;
                },
                ( partialSum ) => { Interlocked.Add( ref k, partialSum ); } );
            sw.Stop();

            Console.WriteLine( "Wynik: {0} - {1} ms", 4.0 * k / n, sw.ElapsedMilliseconds );
        }
    }
}
