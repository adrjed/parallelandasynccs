﻿using System;
using System.Runtime.Remoting.Contexts;
using System.Threading;

namespace ParallelAndAsyncCS.JAlbahari2
{
    [Synchronization]
    public class AutoLock : ContextBoundObject
    {
        public void Demo()
        {
            Console.Write( "Start..." );
            Thread.Sleep( 1000 );           // We can't be preempted here
            Console.WriteLine( "end" );     // thanks to automatic locking!
        }

        public void Test()
        {
            new Thread( Demo ).Start();
            new Thread( Demo ).Start();
            new Thread( Demo ).Start();
            Console.ReadLine();
        }

        public static void MainRun()
        {
            new AutoLock().Test();
        }
    }

    public class Test
    {
        public static void MainRun()
        {
            AutoLock safeInstance = new AutoLock();
            new Thread( safeInstance.Demo ).Start();    // Call the Demo
            new Thread( safeInstance.Demo ).Start();    // method 3 times
            safeInstance.Demo();                        // concurrently
        }
    }

    [Synchronization]
    public class Deadlock : ContextBoundObject
    {
        public Deadlock Other;
        public void Demo()  { Thread.Sleep( 1000 ); Other.Hello(); }
        void Hello()        { Console.WriteLine( "Hello" ); }
    }

    public class Test2
    {
        public static void Main0()
        {
            Deadlock dead1 = new Deadlock();
            Deadlock dead2 = new Deadlock();
            dead1.Other = dead2;
            dead2.Other = dead1;
            new Thread( dead1.Demo ).Start();
            dead2.Demo();
        }
    }
}
