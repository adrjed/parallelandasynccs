﻿using AJ.Reflection;
using System;
using static System.Console;
using System.Threading;

namespace ParallelAndAsyncCS.JAlbahari2
{
    [CInf(21,1,"Delelagat asynchroniczny, pula wątków TheadPool")]
    public class AsyncDelegates
    {
        // FIXME: troche nie w tym miejscu
        [FInf(21,1,"Wywoływanie asynchroniczne za pomocą puli wątków")]
        public void ThreadPoolTest()
        {
            ThreadPool.QueueUserWorkItem( TPTestMethod );
            ThreadPool.QueueUserWorkItem( TPTestMethod, 123 );
        }

        public void TPTestMethod(object data)
        {
            WriteLine( "Hello from the thread pool! " + data );
        }

        EventWaitHandle _waitHandle = new AutoResetEvent( false );

        [FInf(21,2,"Wywołanie asynchroniczne za pomocą delegata")]
        public void AsyncDelegateTest()
        {
            Func<string, int> method = Work1;
            IAsyncResult cookie = method.BeginInvoke( "test", Work1Done, method );

            for ( int i = 0 ;i < 10 ;i++ )
                WriteLine( "Inna robota {0}", i );

            if ( cookie.IsCompleted )
                WriteLine( "Work1 zakonczona przed odwolaniem" );
            else
            {
                WriteLine( "Czekamy na Work1" );
                _waitHandle.WaitOne();
                while ( !cookie.IsCompleted )
                    Write( "." );
            }

            //int result = method.EndInvoke( cookie );

            
        }

        public int Work1 (string s)
        {
            Thread.Sleep( 100 );
            _waitHandle.Set();
            return s.Length;
        }

        public void Work1Done(IAsyncResult cookie)
        {
            var target = (Func<string, int>)cookie.AsyncState;
            int result = target.EndInvoke( cookie );
            WriteLine( "Work1Done! result = {0}", result );
        }
    }

}
