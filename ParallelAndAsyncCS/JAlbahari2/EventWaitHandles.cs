﻿using AJ.Reflection;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ParallelAndAsyncCS.JAlbahari2.EventWaitHandles
{
    [CInf(20, 1, "Podstawowa obsługa oczekiwania na zdarzenie przez wątki")]
    public class BasicWaitHandle
    {
        static EventWaitHandle _waitHandle = new AutoResetEvent( false );

        [FInf(20,1,"Uruchamianie EventWaitHandle")]
        public void MainRun()
        {
            new Thread( Waiter ).Start();
            new Thread( SecondWaiter ).Start();
            Thread.Sleep( 1000 );       // Zatrzymaj na sekundę...
            _waitHandle.Set();          // Wyślij sygnał do czekającego
            _waitHandle.Set();          // Wyślij do kolejnego czekającego
        }

        static void Waiter()
        {
            Console.WriteLine( "Waiting..." );
            _waitHandle.WaitOne();
            Console.WriteLine( "Notified. Thread running after _waitHandle.Set() invoked from another Thread." );
        }
        // dodatkowe, może nie działać
        static void SecondWaiter()
        {
            Console.WriteLine( "Waiter2 waiting for event..." );
            _waitHandle.WaitOne();
            Console.WriteLine( "Waiter2 notified" );
        }
    }

    [CInf(20, 2, "Obsługa obustronnego oczekiwania między wątkami na zdarzenia wznowienia pracy" )]
    public class TwoWaySignaling
    {
        static EventWaitHandle _ready = new AutoResetEvent( false );
        static EventWaitHandle _go = new AutoResetEvent( false );
        static readonly object _locker = new object();
        static string _message;

        [FInf( 20, 2, "Oczekiwanie wątku głównego na robocze i roboczych na główny" )]
        public void MainRun()
        {
            new Thread( Work ).Start();

            _ready.WaitOne();       // Poczekaj, aż wątek roboczy wyśle sygnał
            lock ( _locker ) _message = "first message";
            _go.Set();              // Sygnał do wątku roboczego, aby kontynuował

            _ready.WaitOne();       // Znów czekaj na gotowośc wątku roboczego
            lock ( _locker ) _message = "second message";
            _go.Set();

            _ready.WaitOne();
            lock ( _locker ) _message = null;// Wątek roboczy zakończy się
            _go.Set();
        }

        static void Work()
        {
            while(true)
            {
                _ready.Set();       // Ogłoś, że wątek roboczy gotowy
                _go.WaitOne();      // Czekaj na sygnał z innego wątku
                lock(_locker)
                {
                    if ( _message == null ) return;
                    Console.WriteLine("Thread " + Thread.CurrentThread.ManagedThreadId + " :" + _message );
                }
            }
        }
    }

    class ProducerConsumerQueue : IDisposable
    {
        EventWaitHandle _wh = new AutoResetEvent( false );
        Thread _worker;
        readonly object _locker = new object();
        Queue<string> _tasks = new Queue<string>();

        public ProducerConsumerQueue()
        {
            _worker = new Thread( Work );
            _worker.Start();
        }

        public void EnqueueTask(string task)
        {
            lock ( _locker ) _tasks.Enqueue( task );
            _wh.Set();
        }

        void Work()
        {
            while(true)
            {
                string task = null;
                lock(_locker)
                {
                    if(_tasks.Count > 0)
                    {
                        task = _tasks.Dequeue();
                        if ( task == null ) return;
                    }
                }
                if ( task != null )
                {
                    Console.WriteLine( "Performing task: " + task );
                    Thread.Sleep( 1000 ); // symuluj pracę...
                }
                else
                    _wh.WaitOne();
            }
        }

        public void Dispose()
        {
            EnqueueTask( null );
            _worker.Join();
            _wh.Close();
        }
    }

    [CInf(20,3,"Kolejka Producent-Konsument")]
    public class ProducerConsumerQueueTest
    {
        [FInf(20,3,"Testowanie kolejki")]
        public static void MainRun()
        {
            using(var q = new ProducerConsumerQueue())
            {
                q.EnqueueTask( "Hello" );
                for ( int i = 0 ;i < 10 ;i++ ) q.EnqueueTask( "Say " + i );
                q.EnqueueTask( "Goodbye!" );
            }
            // Wyjście z deklaracji using wywołuje metodę Dispose() obiektu q,
            // kolejkuje puste zadanie i czeka na koniec pracy konsumenta
        }
    }

    // Wspomnienie o ManualResetEvent, ManualResetEventSlim

    [CInf(20,4, "Wątek wznawiany po określonej liczbie wywołań Signal() - CountdownEvent")]
    public class CountDownEventTest
    {
        static CountdownEvent _countdown = new CountdownEvent( 3 );
        static int SleepTime;

        [FInf(20,4,"Testowanie")]
        public static void MainRun()
        {
            new Thread( SaySomething ).Start( "Jestem wątkiem 1" );
            new Thread( SaySomething ).Start( "Jestem wątkiem 2" );
            new Thread( SaySomething ).Start( "Jestem wątkiem 3" );

            _countdown.Wait(); // Blokuje ten wątek dopóki Signal() będzie wywołany 3 razy
            Console.Write( "Wszystkie wątki zakończyły działanie!" );
            _countdown.Reset();
        }

        static void SaySomething(object thing)
        {
            Thread.Sleep( Interlocked.Add(ref SleepTime, 500) );
            Console.WriteLine( thing );
            _countdown.Signal();
        }
    }

    [CInf(20,5,"ManualResetEventTest")]
    public class ManualResetEventTest
    {
        static ManualResetEvent _starter = new ManualResetEvent( false );

        [FInf(20,5,"ManualResetEventTest")]
        public static void ManinRun()
        {
            RegisteredWaitHandle reg = ThreadPool.RegisterWaitForSingleObject
                ( _starter, Go, "Some data", -1, true );

            Thread.Sleep( 5000 );
            Console.WriteLine( "Powiadamianie zadania roboczego..." );
            _starter.Set();
            Console.ReadLine();
            reg.Unregister( _starter ); // Posprzątaj, gdy gotowe
        }

        public static void Go (object data, bool timedOut)
        {
            Console.WriteLine( "Started - " + data );
            // Przeprowadź operacje
        }
    }

}
