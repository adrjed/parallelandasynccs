﻿namespace ParallelAndAsyncCS.Section11
{
    /*
     * 1. Zaobserwuj w oknie wątków działanie programu obliczającego liczbę Pi (listing 7.14),
     *  ustaw breakpoint w miejscu polecenia sumaCzesciowa++) i odpowiedz na pytania:
     * a) ile wątków roboczych zostało utworzonych?
     * b) ile spośród tych wątków wykonuje motody programu ?
     * c) ile wątków jednocześnie może wykonywać wybrane polecenie ?
     * 2. Przeanalizuj w oknie ParallelWatch wartości cześciowe liczby Pi (zwracane przez metodę
     *  statyczną) w różnych wątkach w programie z listingu 2.9.
     * 3. Przeanalizuj działąnie programu z listingu 2.9 z wykorzystaniem Concurrency Visualizer
     * 4. W rozdziale 5. opisany jest prosty program, w których dodatkowy wątek zmienia położenie
     *  paska postępu (w wersji opartej na Constorl.Invoke i kontekście synchronizacji). Dodanie
     *  na końcu metody zdarzeniowej tworzącej ten dodatkowy wątek polecenia Join powoduje
     *  "zastygnięcie" programu. Prześledź tę sytuację za pomocą narzędzi debugowania.
     */
    class Section11Zadania
    {
    }
}
