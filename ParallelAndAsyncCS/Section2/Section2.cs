﻿using AJ.Reflection;
using System;
using System.Threading;


namespace ParallelAndAsyncCS.Section2
{
    /// <summary>Rozdział 2 - Wątki/// </summary>
    [CInf(2, 1, "Wątki")]
    public class Section_2
    {
        public static Random r = new Random();

        /// <summary>Listing 2.1.1 - Uruchamianie kodu niezrównoleglonego</summary>
        [FInf( 2, 1, 1, "Uruchamianie kodu niezrównoleglonego" )]
        public void Main_2_1()
        {
            uruchamianieObliczenPi_2_1();
        }

        /// <summary>Listing 2.1.2 - kod niezrównoleglony</summary>
        [FInf(2, 1, 2, "Kod niezrównoleglony 1")]
        public double obliczPi(long iloscProb = 10)
        {
            double x,y;
            long iloscTrafien = 0;

            for(long i=0; i< iloscProb; ++i)
            {
                x = r.NextDouble();
                y = r.NextDouble();
                if (x*x + y*y < 1) ++iloscTrafien;
                // Console.WriteLine("x={0}, y={1}", x, y);
            }

            return 4.0 * iloscTrafien / iloscProb;
        }

        /// <summary>Listing 2.1.3 - kod niezrownoleglony</summary>
        [FInf(2, 1, 3, "Kod niezrównoleglony 2")]
        public void uruchamianieObliczenPi_2_1()
        {
            int czasPoczatkowy = Environment.TickCount;

            long iloscProb = 100000000L;
            double pi = obliczPi(iloscProb);
            Console.WriteLine("Pi={0}, błąd={1}", pi, Math.Abs(Math.PI - pi));

            int czasKoncowy = Environment.TickCount;
            int roznica = czasKoncowy - czasPoczatkowy;
            Console.WriteLine("Czas obliczen: " + roznica.ToString());
        }

        /// <summary>Listing 2.2.1 - tworzenie i uruchamianie wątku</summary>
        [FInf(2, 2, 1, "Tworzenie i uruchamianie wątku")]
        public void Main_2_2()
        {
            Thread t = new Thread( uruchamianieObliczenPi_2_2 );
            t.Start();
            Console.WriteLine( "Czy ten napis pojawi się przed otrzymaniem wyniku?" );
        }

        /// <summary>Listing 2.2.2 - tworzenie i uruchamianie wątku</summary>
        [FInf(2, 2, 2, "Tworzenie i uruchamianie wątku - kod ")]
        public void uruchamianieObliczenPi_2_2()
        {
            int czasPoczatkowy = Environment.TickCount;

            Console.WriteLine( "Uruchamianie obliczen, wątek nr {0}...", Thread.CurrentThread.ManagedThreadId );

            long iloscProb = 100000000L;
            double pi = obliczPi( iloscProb );
            Console.WriteLine( "Pi={0}, błąd={1}, wątek nr {2}", pi, Math.Abs( Math.PI - pi ), Thread.CurrentThread.ManagedThreadId );

            int czasKoncowy = Environment.TickCount;
            int roznica = czasKoncowy - czasPoczatkowy;
            Console.WriteLine( "Czas obliczeń: " + roznica.ToString() );
        }

        ///<summary>Listing 2.3 - Użycie metody Abort spowoduje wywołanie wyjątku w wątku</summary>
        [FInf(2, 3, "Użycie metody Abort spowoduje wywołanie wyjątku w wątku")]
        public void uruchamianieObliczenPi_2_3()
        {
            try
            {
                int czasPoczatkowy = Environment.TickCount;
                Console.WriteLine( "Uruchamianie obliczeń, wątek nr {0}...", Thread.CurrentThread.ManagedThreadId );

                long iloscProb = 100000000L;
                double pi = obliczPi( iloscProb );
                Console.WriteLine( "Pi={0}, błąd={1}, wątek nr {2}", pi, Math.Abs( Math.PI - pi ), Thread.CurrentThread.ManagedThreadId );

                int czasKoncowy = Environment.TickCount;
                int roznica = czasKoncowy - czasPoczatkowy;
                Console.WriteLine( "Czas obliczen: " + roznica.ToString() );
            }
            catch (ThreadAbortException exc)
            {
                Console.WriteLine( "Działanie wątku zostało przerwane (" + exc.Message + ")" );
            }
            catch(Exception exc)
            {
                Console.WriteLine( "Wyjątek (" + exc.Message + ")" );
            }
        }

        /// <summary>Listing 2.4 - Wywołanie metody Abory pół sekundy po uruchomieniu obliczeń</summary>
        [FInf(2, 4, "Wywołanie metody Abort pół sekundy po uruchomieniu obliczeń")]
        public void Main_2_4()
        {
            Thread t = new Thread(uruchamianieObliczenPi_2_3);
            t.Start();
            Thread.Sleep( 500 );
            t.Abort();
            Console.WriteLine( "Czy ten napis pojawi się przed otrzymaniem wyniku?" );
        }

        /// <summary>Listing 2.5 - Wątki tła nie blokują zamknięcia procesu</summary>
        [FInf(2,5, "Wątki tła nie blokują zamknięcia procesu")]
        public void Main_2_5()
        {
            Thread t = new Thread( uruchamianieObliczenPi_2_3 );
            t.IsBackground = true;
            t.Start();
            Thread.Sleep( 500 );
        }

        /// <summary>Listine 2.6 - Zmiana priorytetu wątku</summary>
        [FInf(2, 6, "Zmiana priorytetu wątku")]   
        public void Main_2_6()
        {
            Thread t = new Thread( uruchamianieObliczenPi_2_3 );
            t.Priority = ThreadPriority.Highest;
            t.Start();
        }

        ///<summary>Listing 2.7 - Uruchamianie obliczeń PI</summary>
        //[FInf( 2, 7, 0, "Uruchamianie obliczen PI" )]
        public void uruchamianieObliczenPi_2_7(object ilewatkow)
        {
            int ileWatkow = (int)ilewatkow;

            try
            {
                int czasPoczatkowy = Environment.TickCount;
                Console.WriteLine( "Uruchamianie obliczeń, wątek nr {0}...", Thread.CurrentThread.ManagedThreadId );

                long iloscProb = 100000000L / ileWatkow;
                double pi = obliczPi_2_8( iloscProb );
                Console.WriteLine( "Pi={0}, błąd={1}, wątek nr {2}", pi, Math.Abs( Math.PI - pi ), Thread.CurrentThread.ManagedThreadId );

                int czasKoncowy = Environment.TickCount;
                int roznica = czasKoncowy - czasPoczatkowy;
                Console.WriteLine( "Czas obliczen: " + roznica.ToString() );
            }
            catch ( ThreadAbortException exc )
            {
                Console.WriteLine( "Działanie wątku zostało przerwane (" + exc.Message + ")" );
            }
            catch ( Exception exc )
            {
                Console.WriteLine( "Wyjątek (" + exc.Message + ")" );
            }
        }

        /// <summary>Listing 2.7 - Dziesięć wątków z najniższym priorytetem</summary>
        [FInf(2, 7, "Dziesięć wątków z najniższym priorytetem")]
        public void Main_2_7()
        {
            const int ileWatkow = 10;

            Thread[] tt = new Thread[ileWatkow];
            for( int i =0; i < ileWatkow; i++)
            {
                tt[i] = new Thread( uruchamianieObliczenPi_2_7 );
                tt[i].Priority = ThreadPriority.Lowest;
                tt[i].Start( ileWatkow );
            }
        }

        /// <summary>Listing 2.8 - Tworzenie osobnych instancji genereatora liczb pseudolosowych w każdym wątku</summary>
        //[FInf( 2, 8, "Kod obliczania PI dla wielu wątków" )]
        public double obliczPi_2_8( long iloscProb = 10 )
        {
            Random r = new Random( Section_2.r.Next() & DateTime.Now.Millisecond );
            double x, y;
            long iloscTrafien = 0;

            for ( long i = 0 ;i < iloscProb ;++i )
            {
                x = r.NextDouble();
                y = r.NextDouble();
                if ( x * x + y * y < 1 ) ++iloscTrafien;
                // Console.WriteLine("x={0}, y={1}", x, y);
            }
            return 4.0 * iloscTrafien / iloscProb;
        }

    }

    [CInf(2, 9, "Łączenie wątków")]
    public class Section_2_9
    {
        static double pi = 0;
        const int ileWatkow = 10;

        public double obliczPi_2_9( long iloscProb = 10 )
        {
            Random r = new Random( Section_2.r.Next() & DateTime.Now.Millisecond );
            double x, y;
            long iloscTrafien = 0;

            for ( long i = 0 ;i < iloscProb ;++i )
            {
                x = r.NextDouble();
                y = r.NextDouble();
                if ( x * x + y * y < 1 ) ++iloscTrafien;
                // Console.WriteLine("x={0}, y={1}", x, y);
            }
            return 4.0 * iloscTrafien / iloscProb;
        }

        ///<summary>Listing 2.9 - Uruchamianie obliczeń PI</summary>
        public void uruchamianieObliczenPi_2_9()
        {
            try
            {
                int czasPoczatkowy = Environment.TickCount;
                Console.WriteLine( "Uruchamianie obliczeń, wątek nr {0}...", Thread.CurrentThread.ManagedThreadId );

                long iloscProb = 100000000L / ileWatkow;
                double pi = obliczPi_2_9( iloscProb );
                Section_2_9.pi += pi;
                Console.WriteLine( "Pi={0}, błąd={1}, wątek nr {2}", pi, Math.Abs( Math.PI - pi ), Thread.CurrentThread.ManagedThreadId );

                int czasKoncowy = Environment.TickCount;
                int roznica = czasKoncowy - czasPoczatkowy;
                Console.WriteLine( "Czas obliczen: " + roznica.ToString() );
            }
            catch ( ThreadAbortException exc )
            {
                Console.WriteLine( "Działanie wątku zostało przerwane (" + exc.Message + ")" );
            }
            catch ( Exception exc )
            {
                Console.WriteLine( "Wyjątek (" + exc.Message + ")" );
            }
        }

        /// <summary>Użycie matody Join</summary>
        [FInf( 2, 9, "Użycie metody Join" )]
        public void Main_2_9()
        {
            int czasPoczatkowy = Environment.TickCount;

            Thread[] tt = new Thread[ileWatkow];
            for(int i = 0; i < ileWatkow; i++)
            {
                tt[i] = new Thread( uruchamianieObliczenPi_2_9 );
                tt[i].Priority = ThreadPriority.Lowest;
                tt[i].Start();
            }

            //oczekiwanie na zakończenie wątków
            foreach(Thread t in tt)
            {
                t.Join();
                Console.WriteLine( "Zakończył działanie wątek nr {0}", t.ManagedThreadId );
            }
            pi /= ileWatkow;
            Console.WriteLine( "Wszystkie wątki zakończyły działanie.\nUśrednione Pi = {0}, błąd = {1}",
            pi, Math.Abs( Math.PI - pi ) );

            int czasKoncowy = Environment.TickCount;
            int roznica = czasKoncowy - czasPoczatkowy;
            Console.WriteLine( "Czas obliczeń: {0}", roznica );
        }
    }

    [CInf(2, 10, "Sekcje krytyczne (lock)")]
    public class Section_2_10
    {
        static Random r = new Random();
        static double pi = 0;
        const int ileWatkow = 10;
        public double obliczPi_2_10( long iloscProb = 10 )
        {
            Random r = new Random( Section_2.r.Next() & DateTime.Now.Millisecond );
            double x, y;
            long iloscTrafien = 0;

            for ( long i = 0 ;i < iloscProb ;++i )
            {
                if ( i == iloscProb / 2)
                {
                    lock((object)Section_2_10.pi) //pudełkowanie
                    {
                        Console.WriteLine( "Synchronizacja: wątek nr {0} osiągnął półmetek",
                            Thread.CurrentThread.ManagedThreadId );
                    }
                }
                x = r.NextDouble();
                y = r.NextDouble();
                if ( x * x + y * y < 1 ) ++iloscTrafien;
            }
            return 4.0 * iloscTrafien / iloscProb;
        }

        ///<summary>Listing 2.10 - Uruchamianie obliczeń PI</summary>
        [FInf(2, 10, "Synchronizacja wątków w połowie obliczeń")]
        public void uruchamianieObliczenPi_2_10()
        {
            try
            {
                int czasPoczatkowy = Environment.TickCount;
                Console.WriteLine( "Uruchamianie obliczeń, wątek nr {0}...", Thread.CurrentThread.ManagedThreadId );

                long iloscProb = 100000000L / ileWatkow;
                double pi = obliczPi_2_10( iloscProb );
                lock ( r )
                { Section_2_10.pi += pi; }
                Console.WriteLine( "Pi={0}, błąd={1}, wątek nr {2}", pi, Math.Abs( Math.PI - pi ), Thread.CurrentThread.ManagedThreadId );

                int czasKoncowy = Environment.TickCount;
                int roznica = czasKoncowy - czasPoczatkowy;
                Console.WriteLine( "Czas obliczen: " + roznica.ToString() );
            }
            catch ( ThreadAbortException exc )
            {
                Console.WriteLine( "Działanie wątku zostało przerwane (" + exc.Message + ")" );
            }
            catch ( Exception exc )
            {
                Console.WriteLine( "Wyjątek (" + exc.Message + ")" );
            }
        }
    }

    [CInf( 2, 11, "Przesyłanie danych do wątku" )]
    public class Section_2_11
    {
        static Random r = new Random();
        static double pi = 0;
        const int ileWatkow = 10;

        public void Main_2_11()
        {
            int time = Environment.TickCount;
            // tworzenie wątków
            Thread[] tt = new Thread[ileWatkow];
            for( int i = 0; i < ileWatkow; ++i)
            {
                tt[i] = new Thread( uruchamianieObliczenPi_2_11 );
                tt[i].Priority = ThreadPriority.Lowest;
                tt[i].Start(i);
            }

            foreach(Thread t in tt){
                t.Join();
                Console.WriteLine("Zakończył działanie wątek nr {0}", t.ManagedThreadId);
            }

            pi /= ileWatkow;
            Console.WriteLine("Wszystkie wątki zakończyły działanie.\nUśrednione Pi={0}, błąd={1}", pi, Math.Abs(Math.PI - pi));
            time = Environment.TickCount - time;;
            Console.WriteLine("Czas obliczeń: {0}", time.ToString());
        }

        public double obliczPi_2_11( long iloscProb = 10 )
        {
            Random r = new Random( Section_2.r.Next() & DateTime.Now.Millisecond );
            double x, y;
            long iloscTrafien = 0;

            for ( long i = 0 ;i < iloscProb ;++i )
            {
                if ( i == iloscProb / 2 )
                {
                    lock ( (object)Section_2_11.pi ) //pudełkowanie
                    {
                        Console.WriteLine( "Synchronizacja: wątek nr {0} osiągnął półmetek",
                            Thread.CurrentThread.ManagedThreadId );
                    }
                }
                x = r.NextDouble();
                y = r.NextDouble();
                if ( x * x + y * y < 1 ) ++iloscTrafien;
            }
            return 4.0 * iloscTrafien / iloscProb;
        }

        ///<summary>Listing 2.10 - Uruchamianie obliczeń PI</summary>
        [FInf( 2, 11, "Synchronizacja wątków w połowie obliczeń" )]
        public void uruchamianieObliczenPi_2_11(object parameter)
        {
            try
            {
                int czasPoczatkowy = Environment.TickCount;
                int? indeks = parameter as int?;
                Console.WriteLine( "Uruchamianie obliczeń, wątek nr {0}, indeks{1}...", Thread.CurrentThread.ManagedThreadId, indeks.HasValue?indeks.Value.ToString():"---" );

                long iloscProb = 100000000L / ileWatkow;
                double pi = obliczPi_2_11( iloscProb );
                Section_2_11.pi += pi;
                Console.WriteLine( "Pi={0}, błąd={1}, wątek nr {2}", pi, Math.Abs( Math.PI - pi ), Thread.CurrentThread.ManagedThreadId );

                int czasKoncowy = Environment.TickCount;
                int roznica = czasKoncowy - czasPoczatkowy;
                Console.WriteLine( "Czas obliczen: " + roznica.ToString() );
            }
            catch ( ThreadAbortException exc )
            {
                Console.WriteLine( "Działanie wątku zostało przerwane (" + exc.Message + ")" );
            }
            catch ( Exception exc )
            {
                Console.WriteLine( "Wyjątek (" + exc.Message + ")" );
            }
        }
    }

    [CInf(2, 12, "Zmiany w metodzie Main wprowadzone w celu utworzenia puli wątków")]
    public class Section_2_12
    {
        [FInf(2, 12,"Obliczanie Pi oparte o pulę wątków")]
        public void Main_2_12()
        {
            int time = Environment.TickCount;
            Random r = new Random();
            double PI = 0;
            int ileWatkow = 10;
            

            Func<long, double> obliczPi = ( iloscProb ) =>
            {
                Random localrand = new Random( Section_2.r.Next() & DateTime.Now.Millisecond );
                double x, y;
                long iloscTrafien = 0;

                for ( long i = 0 ;i < iloscProb ;++i )
                {
                    if ( i == iloscProb / 2 )
                    {
                        lock ( (object)PI ) //pudełkowanie
                        {
                            Console.WriteLine( "Synchronizacja: wątek nr {0} osiągnął półmetek",
                                Thread.CurrentThread.ManagedThreadId );
                        }
                    }
                    x = localrand.NextDouble();
                    y = localrand.NextDouble();
                    if ( x * x + y * y < 1 ) ++iloscTrafien;
                }
                return 4.0 * iloscTrafien / iloscProb;
            };
            
            Action<object> uruchamianieObliczenPi = (object parameter) =>
            {
                try
                {
                    int czas = Environment.TickCount;
                    int? indeks = parameter as int?;
                    Console.WriteLine( "Uruchamianie obliczeń, wątek nr {0}, indeks{1}...", 
                        Thread.CurrentThread.ManagedThreadId, 
                        indeks.HasValue?indeks.Value.ToString():"---" );

                    long iloscProb = 100000000L / ileWatkow;
                    double pi = obliczPi( iloscProb );
                    PI += pi;
                    Console.WriteLine( "Pi={0}, błąd={1}, wątek nr {2}", pi, 
                        Math.Abs( Math.PI - pi ), 
                        Thread.CurrentThread.ManagedThreadId );

                    czas = Environment.TickCount - czas;
                    Console.WriteLine( "Czas obliczen: " + czas.ToString() );
                }
                catch ( ThreadAbortException exc ){
                    Console.WriteLine( "Działanie wątku zostało przerwane (" + exc.Message + ")" );}
                catch ( Exception exc ){
                    Console.WriteLine( "Wyjątek (" + exc.Message + ")" );}
            };

            //tworzenie wątków
            WaitCallback threadMethod = new WaitCallback( uruchamianieObliczenPi );
            ThreadPool.SetMaxThreads( 30, 100 );
            for ( int i = 0 ;i < ileWatkow ;++i )
                ThreadPool.QueueUserWorkItem( threadMethod, i );

            //czekanie na zakończenie wątków
            int availableThreadsInPoolNum = 0,  // nieużywane wątki w puli
                allThreadsInPoolNum = 0,        // wszystkie wątki w puli
                workingThreadsInPoolNum = 0,    // używane wątki w puli
                tmp = 0;
            do
            {
                ThreadPool.GetAvailableThreads( out availableThreadsInPoolNum, out tmp );
                ThreadPool.GetMaxThreads( out allThreadsInPoolNum, out tmp );
                workingThreadsInPoolNum = allThreadsInPoolNum - availableThreadsInPoolNum;
                Console.WriteLine( "Ilość aktywnych wątków puli: {0}", workingThreadsInPoolNum );
                Thread.Sleep( 1000 );
            } while ( workingThreadsInPoolNum > 0 );
            PI /= ileWatkow;
            Console.WriteLine( "Wszystkie wątki zakończyły działanie.\nUśrednione Pi={0}, błąd={1}", PI,
                Math.Abs( Math.PI - PI ) );
            time = Environment.TickCount - time;
            Console.WriteLine( "Czas obliczeń: " + time.ToString() );
        }
    }

    [CInf(2, 15, "Operacje atomowe")]
    public class Section_2_15
    {
        [FInf(2, 15, "Użycie klasy Syste.Threading.Timer")]
        public void Listing_2_15()
        {
            const int threadsNum = 100;
            const long trialsNumInThread = 10000000L;
            long overallTrialNum = 0L;

            EventWaitHandle[] ewht = new EventWaitHandle[threadsNum];

            System.Timers.Timer timer = new System.Timers.Timer(1000);
            timer.Elapsed += ( object sender, System.Timers.ElapsedEventArgs e ) =>
            {
                Console.WriteLine( "Ilość prób: " + Interlocked.Read( ref overallTrialNum ).ToString()
                    + "/" + (threadsNum * trialsNumInThread).ToString() );
            };
            timer.Start();

            int time = Environment.TickCount;
            

            Func<long, long> obliczPi = ( iloscProb ) =>
            {
                Random localrand = new Random( Section_2.r.Next() & DateTime.Now.Millisecond );
                double x, y;
                long iloscTrafien = 0;

                for ( long i = 0 ;i < iloscProb ;++i )
                {
                    x = localrand.NextDouble();
                    y = localrand.NextDouble();
                    if ( x * x + y * y < 1 ) ++iloscTrafien;
                    //Console.WriteLine("x={0}, y={1}", x, y);
                }
                return iloscTrafien;
            };

            Action<object> uruchamianieObliczenPi = ( object parameter ) =>
            {
                try
                {
                    int czas = Environment.TickCount;
                    int? indeks = parameter as int?;
                    Console.WriteLine( "Uruchamianie obliczeń, wątek nr {0}, indeks{1}...",
                        Thread.CurrentThread.ManagedThreadId,
                        indeks.HasValue ? indeks.Value.ToString() : "---" );

                    long iloscTrafien = obliczPi( trialsNumInThread );
                    Interlocked.Add(ref overallTrialNum, iloscTrafien);
                    czas = Environment.TickCount - czas;
                    Console.WriteLine( "Czas obliczen: " + czas.ToString() );
                }
                catch ( ThreadAbortException exc )
                {
                    Console.WriteLine( "Działanie wątku zostało przerwane (" + exc.Message + ")" );
                }
                catch ( Exception exc )
                {
                    Console.WriteLine( "Wyjątek (" + exc.Message + ")" );
                }
            };

            //tworzenie wątków
            WaitCallback threadMethod = new WaitCallback( uruchamianieObliczenPi );
            ThreadPool.SetMaxThreads( 30, 100 );
            for ( int i = 0 ;i < threadsNum ;++i )
            {
                ewht[i] = new EventWaitHandle( false, EventResetMode.AutoReset );
                ThreadPool.QueueUserWorkItem( threadMethod, i );
            }

            for(int i = 0; i< threadsNum; ++i)
                ewht[i].WaitOne();

            double PI = 4.0 * overallTrialNum / (trialsNumInThread * threadsNum);
            Console.WriteLine( "Wszystkie wątki zakończyły działanie.\nUśrednione Pi={0}, błąd={1}", PI,
                Math.Abs( Math.PI - PI ) );
            time = Environment.TickCount - time;
            Console.WriteLine( "Czas obliczeń: " + time.ToString() );
        }
    }
}
