﻿namespace ParallelAndAsyncCS.Section2
{
    /*
     * 1.Zmodyfikuj program do obliczania przybliżenia liczby Pi w taki sposób,
     * aby wprowadzić do niego flagę pozwalającą na wstrzymanie i wznowienie
     * wszystkich wątków, w których przeprowadzane są obliczenia. Ja zmienić
     * program, aby możliwe było wstrzymywanie każdego wątku osobno?
     * 
     * 2. W programie służącym do obliczania przybliżenia liczby Pi użyj klasy
     * CancellationTokenSource i struktury CencellationToken do przerwania
     * działania wszystkich wątków jednocześnie (rozdział 6.). Sprawdź zarówno
     * zgłaszanie wyjątkó typu OperationCanceledException meotodą CencellationToken
     * ThrowIfCalcellationRequested w razie wywołania metody CancellatioToken
     * Source.Cancel (rozwiązanie podobne do działania metody Thread.Abort),
     * jak i instrukcję warunkową sprawdzającą, czy metoda ta została wywołana
     * (własność IsCancellationRequested).
     * 
     * 3. Utwórz instancję poniżej klasy, a następnie uruchom jej metodę w trzech
     * wątkach. Oczywiście, powinny być wykonane jednocześnie. Następnie wskaż
     * klasę bazową ContextBoundObject i dodaj atrybut Synchronization. Sprawdź,
     * że teraz megody, pomimo tworzenia wątków, wykonywane są sekwencyjnie.
     * 
     * public class Klasa
     * {
     *      public void Metoda()
     *      {
     *          Console.WriteLine("Początek: " + Thread.CurrentThread.ManagedThreadId);
     *          Thread.Sleep(1000);
     *          Console.WriteLine("Koniec: " + Thread.CurrentThread.ManagedThreadId);
     *      }
     * }
     * 
     * 4. Utwórz aplikację z swoma wątkami. Korzystając z obiektów klasy
     * EventWaitHandle, skonfiguruj je tak, żeby każdy z nich przez sekundę
     * drukował w konsoli swój nymer wątku. Uruchomienie jednego z wątków 
     * powinno powodować zatrzymanie drugiego. I odwrotnie.
     * 
     * 5. Przygotuj klasę parametryczną, która, otaczając prostą zmienną zdefiniowaną
     * jako pole tej klasy (typu wskazanego przez parametr), przejmie obowiązek
     * zabezpieczeni odczytu lub przypisania. Należy w tym celu zdefiniować
     * własność BezpiecznaWartość, która w sekcjach get i set będzie korzystała z sekcji
     * krytycznych tworzonych poleceniem lock. Dlaczego takie rozwiązanie nie ma 
     * sensu ?
     * 
     * 6. Napisz program przeszukujący tysiącelementową tablicę w poszukiwaniu
     * minimalnej i maksymalnej wartości. PRzyspiesz sziałanie oweg programu,
     * korzystając z wielu wątków w najprostrzy sposób, tj. dzieląc zakres
     * przeszukiwanych komórek równo pomiędzy wątki. Wykorzystaj tylko tyle
     * wątków, ilejest dostęnych rdzeni procesora.
     */
    class Section2Zadania
    {
    }
}
