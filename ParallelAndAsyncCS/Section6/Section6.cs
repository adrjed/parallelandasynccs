﻿using AJ.Reflection;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelAndAsyncCS.Section6
{
    [CInf(6,"Zadania (tasks)")]
    class Section6
    {
        [FInf(6,1, "Równoległe wykonanie dużej ilości zadań")]
        public void Listing6_1()
        {
            Action a = () =>
                {
                    Console.WriteLine( "Start zadania nr " + Task.CurrentId );
                    Thread.SpinWait( new Random().Next( 100000000 ) );
                    Console.WriteLine( "Koniec zadania nr " + Task.CurrentId );
                };

            Action<object> ao = new Action<object>( ( o ) => { o.ToString(); } );

            List<Task> tList = new List<Task>();

            for ( int i = 0 ;i < 100 ;i++ )
                tList.Add( new Task( a ) );

            tList.ForEach( t => t.Start() );
            tList.ForEach( t => t.Wait() );
        }

        [FInf(6,2, "Zadanie zwracające wartość")]
        public void Listing6_2()
        {
            Task<string> t = new Task<string>( () => { return "Dzień dobry!"; } );

            t.Start();
            t.Wait();

            Console.WriteLine( t.Result );
        }

        [FInf(6,3, "Weryfikacja liczby pierwszej")]
        public void Listing6_3()
        {
            List<Task<int>> list = new List<Task<int>>();
            Console.WriteLine( "Sprawdź, czy liczba jest pierwsza: " );
            int n;
            try
            {
                n = Int32.Parse( Console.ReadLine() );
            }
            catch(Exception ex)
            {
                Console.WriteLine( ex.Message );
                n = 20;
            }

            for ( int i = 2 ;i < (int)Math.Sqrt(n) ;i++ )
            {
                list.Add( new Task<int>( ( j ) =>
                    {
                        if ( n % (int)j == 0 )
                        {
                            return (int)j;
                        }
                        else
                        {
                            return 0;
                        }
                    }, i ) );
            }

            foreach ( Task<int> t in list ) { t.Start(); }
            foreach ( Task<int> t in list ) { t.Wait(); }

            bool isPrime = true;
            foreach(Task<int> t in list)
            {
                if(t.Result != 0)
                {
                    Console.WriteLine( "Liczba {0} dzieli się przez {1}", n, t.Result );
                    isPrime = false;
                }
            }
            if ( isPrime ) Console.WriteLine( "Liczba {0} jest liczbą pierwszą.", n );
        }

        [FInf(6,4,"Przykład użycia metody Task.ContinueWith")]
        public void Listing6_4()
        {
            Task t1, t2, t3;

            t1 = new Task( () => { Thread.Sleep( 1000 ); Console.WriteLine( "zadanie t1 o identyfikatorze {0} zakończone", Task.CurrentId ); } );
            t2 = new Task( () => { Thread.Sleep( 2000 ); Console.WriteLine( "zadanie t2 o identyfikatorze {0} zakończone", Task.CurrentId ); } );
            t3 = new Task( () => { Thread.Sleep( 3000 ); Console.WriteLine( "zadanie t3 o identyfikatorze {0} zakończone", Task.CurrentId ); } );

            Task[] tasks = { t1, t2, t3 };

            t2.ContinueWith( ( t ) =>
            {
                // Task.CurrentId - id aktualnego wątku zadania
                // t.Id - id wątku który został zakończony i przekazany do metody, która po nim miała się rozpocząć
                Console.WriteLine( "Zadanie o identyfikatorze {0} zostało wykonane po zakończeniu zadania t2 o identfikatorze {1}", Task.CurrentId , t.Id);
            } );

            foreach ( Task t in tasks ) t.Start();
            foreach ( Task t in tasks ) t.Wait();
        }

        [FInf(6,5,"Kontynuowanie zadań na przykładzie sztafety")]
        public void Listing6_5()
        {
            Task t1, t2, t3, t4;

            Action a = () =>
                {
                    Console.WriteLine( "Zawodnik nr {0} wystartował", Task.CurrentId );
                    Thread.Sleep( new Random().Next( 1000, 1500 ) );
                    Console.WriteLine( "Zawodnik nr {0} zakończył bieg", Task.CurrentId );
                };

            Action<Task> b = ( t ) =>
                {
                    Console.WriteLine( "Zawodnik nr {0} wystartował po zawodniku nr {1}", Task.CurrentId, t.Id );
                    Thread.Sleep( new Random().Next( 1000, 2500 ) );
                    Console.WriteLine( "Zawodnik nr {0} zakończył bieg", Task.CurrentId );
                };

            t1 = new Task( a );
            t2 = new Task( a );
            t3 = t1.ContinueWith( b );
            t4 = t2.ContinueWith( b );

            t1.Start();
            t2.Start();

            Task.WaitAll( t1, t2, t3, t4 );
            Console.WriteLine( "Wyścig zakończony" );        
        }

        //Listing 6.6 to CancellationTokenSource i CancellationToken

        [FInf(6,7,"Przykład zatrzymania zadania")]
        public void Listing6_7()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            Task t = new Task( () =>
                {
                    try
                    {
                        Console.WriteLine( "Zadanie zostało uruchomione" );
                        for ( ; ; )
                        {
                            ct.ThrowIfCancellationRequested();
                        }
                    }
                    catch(OperationCanceledException oce)
                    {
                        Console.WriteLine( "Zadanie zostało przerwane - " + oce.Message );
                    }
                }, ct );

            t.Start();

            Thread.Sleep( 3000 );
            cts.Cancel();
            t.Wait();
        }

        [FInf(6,8,"Przerywanie oczekiwania na zakończenie zadania")]
        public void Listing6_8()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            Task t = new Task(() =>
            {
                Console.WriteLine( "Zadanie zostało uruchomione" );
                for(;;)
                {
                    Thread.Sleep( 3000 );
                    cts.Cancel();
                }
            });

            t.Start();
            Task[] zadania = { t };

            try
            {
                Task.WaitAll(zadania, ct);
            }
            catch(OperationCanceledException)
            {
                Console.WriteLine( "Przerwano oczekiwanie" );
            }
        }

        //Coś nie działa
        [FInf( 6, 9, "Obsługa zbioru wyjątków typu AggregateException" )]
        public void Listing6_9()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            Task t = new Task( () =>
            {
                Console.WriteLine( "Zadanie zostało uruchomione" );
                for ( ; ; )
                {
                    Thread.Sleep( 3000 );
                    ct.ThrowIfCancellationRequested();
                }
            }, ct );

            t.Start();
            Task[] zadania = { t };

            // Różnica między 6.8 i 6.9 zaczyna się tutaj
            try
            {
                Task.Factory.StartNew( ( obj ) => {
                    CancellationTokenSource ctsource = obj as CancellationTokenSource;
                    ctsource.CancelAfter( 500 );
                }, (object)cts);
                Task.WaitAll( zadania );
            }
            catch( OperationCanceledException ce )
            {
                Console.WriteLine( ce.Message );
            }
            catch ( AggregateException ae )
            {
                foreach ( var exc in ae.InnerExceptions )
                {
                    Console.WriteLine( "Przechwycony wyjątek: {0}", exc.Message );
                }
            }
        }

        [FInf(6,10, "Wyświetlenie kolejnych stanów pojedynczego zadania")]
        public void Listing6_10()
        {
            Task test = new Task( () =>
                {
                    Console.WriteLine( "Zadanie rozpoczęte" );
                    Thread.Sleep( 200 );
                    Console.WriteLine( "Zadanie zakończone" );
                } );

            // zadanie sprawdzające co chwila w jakim stanie jest zadanie testowe
            Task observer = Task.Factory.StartNew( () =>
                {
                    int i = 10;
                    while ( i-- > 0 )
                    {
                        Thread.Sleep( 50 );
                        Console.WriteLine( test.Status.ToString() );
                    }
                } );

            Thread.Sleep( 200 );
            test.Start();

            Task.WaitAll( test, observer );
        }

        [FInf(6,11,"Przerwanie dwóch zadań z uwzględnieniem zmian stanu")]
        public void Listing6_11()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            Task test1 = new Task( () =>
            {
                Console.WriteLine( "\nStart zadania (id:{0})\n", Task.CurrentId );
                while(true)
                {
                    ct.ThrowIfCancellationRequested();
                }
            }, ct, TaskCreationOptions.LongRunning );

            Task test2 = new Task( () =>
            {
                Console.WriteLine( "\nStart zadania (id:{0})\n", Task.CurrentId );
                while ( true )
                {
                    ct.ThrowIfCancellationRequested();
                }
            }, TaskCreationOptions.LongRunning );

            Task observer = Task.Factory.StartNew( () =>
            {
                while(!(test1.IsCompleted && test2.IsCompleted))
                {
                    Thread.Sleep( 50 );
                    Console.WriteLine( "Test1: {0}\t\tTest2: {1}", test1.Status, test2.Status );
                }
            } );

            Thread.Sleep( 200 );
            test1.Start();
            test2.Start();

            while ( test1.Status != TaskStatus.Running || test2.Status != TaskStatus.Running ) ;
            Thread.Sleep( 200 );
            Console.WriteLine( "\nPrzerwanie\n" );
            cts.Cancel();

            try
            {
                Task.WaitAll( test1, test2, observer );
            }
            catch(AggregateException ae)
            {
                Console.WriteLine(" Wyjąteczki : {0}", ae.Message);
                foreach ( var exc in ae.InnerExceptions )
                {
                    Console.WriteLine( "Przechwycono wyjątek: {0}", exc.Message );
                }
            }

            //Console.WriteLine("Naciśnij dowolny przycisk");
            //Console.ReadKey();
        }

        [FInf(6,12,"Tworzenie zadania przez fabrykę obiektów")]
        public void Listing6_12()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            Task<int> t2 = Task<int>.Factory.StartNew( 
                ( o ) =>{Console.WriteLine( o.ToString() ); return 1;},
                (object)"Dzień dobry",
                ct,
                TaskCreationOptions.None,
                TaskScheduler.Default
                );
            t2.Wait();
        }

        [FInf(6,13, "Przykład ze szatfetą rozszerzony o ogłoszenie zwycięzcy")]
        public void Listing6_13()
        {
            Task t1, t2, t3, t4;
            Action a = () =>
                {
                    Console.WriteLine("Zawodnik nr {0} wystartował", Task.CurrentId);
                    Thread.Sleep(new Random().Next(1000, 5000));
                    Console.WriteLine("Zawodnik nr {0} zakończył bieg", Task.CurrentId);
                };

            Action<Task> b = (t) =>
                {
                    Console.WriteLine("Zawodnik nr {0} wystartował po zawodniku nr {1}",
                        Task.CurrentId, t.Id);
                    Thread.Sleep(new Random().Next(1000, 2500));
                    Console.WriteLine("zawodnik nr {0} zakończył bieg", Task.CurrentId);
                };

            t3 = (t1 = Task.Factory.StartNew(a)).ContinueWith(b);
            t4 = (t2 = Task.Factory.StartNew(a)).ContinueWith(b);

            Task[] zadania = {t3, t4};
            Task.Factory.ContinueWhenAny(zadania, (t) => {
                Console.WriteLine("Zawodnik nr {0} wygrał wyścig!", t.Id);
            });

            Task.WaitAll(zadania);
            Console.WriteLine("Wyścig zakończony");
        }

        [FInf(6,14, "Tworzenie własnej fabryki zadań")]
        public void Listing6_14()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken ct = cts.Token;

            TaskFactory tf = new TaskFactory( ct );

            Task a = tf.StartNew( () =>
                {
                    while(true)
                    {
                        Thread.Sleep( 500 );
                        ct.ThrowIfCancellationRequested();
                    }
                } );
            cts.Cancel();

            try
            {
                a.Wait();
            }
            catch(AggregateException)
            {
                Console.WriteLine("Koniec!");
            }
        }

        // 6.16
        class Planista : TaskScheduler
        {
            private List<Task> queue = new List<Task>();
            private Thread mainThread;

            public Planista()
            {
                mainThread = new Thread( () =>
                {
                    Console.WriteLine( "Planista utworzony." );
                    while ( true )
                    {
                        if ( queue.Count > 0 )
                        {
                            int i = new Random().Next( queue.Count );
                            if ( TryExecuteTask( queue[i] ) )
                                queue.RemoveAt( i );
                        }
                    }
                } );
                mainThread.IsBackground = true;
                mainThread.Start();
            }

            public override int MaximumConcurrencyLevel
            {
                get
                {
                    return 1;
                }
            }

            protected override void QueueTask( Task task )
            {
                queue.Add( task );
            }

            protected override bool TryExecuteTaskInline( Task task, bool taskWasPreviouslyQueued )
            {
                Console.WriteLine( "żądanie wykonania sekwencyjnego" );
                if ( Thread.CurrentThread != mainThread ) return false;
                return TryExecuteTask( task );
            }

            protected override IEnumerable<Task> GetScheduledTasks()
            {
                return queue.ToArray();
            }
        }
         
        [FInf(6,17,"Wykorzystanie własnego planisty w zarządzaniu zadaniami")]
        public void Listing6_17()
        {
            Planista ts = new Planista();
            TaskFactory tf = new TaskFactory( ts );

            Task[] tasks = new Task[10];
            for ( int i = 0 ;i < 10 ;i++ )
            {
                Console.WriteLine( "zadanie {0} rozpoczęte", i + 1 );
                tasks[i] = tf.StartNew( ( number ) =>
                    {
                        Console.WriteLine( "Zadanie {0} zakończone", number );
                    }, i+1 );
            }
            Task.WaitAll( tasks, 20000 );

            Console.WriteLine( "Koniec programu!" );
        }


    }
}
