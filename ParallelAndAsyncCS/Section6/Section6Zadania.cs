﻿namespace ParallelAndAsyncCS.Section6
{
    /*
     * 1. Na podstawie programu z listingu 6.16 zaimplementuj własny TaskScheduler,
     * który przydzielać będzie zadania do więcej niż jednego wątku na zasadzie:
     * a) kolejki (FIFO),
     * b) stosu (LIFO).
     * 2. Dodaj do proramu z listunku 6.11 nowe zadanie Test3 oraz powąż je relacją
     * przodek-potomek z zadaniem Test1 tak, aby Test3 był zadaniem nadrzędnym dla 
     * Test1. Zaobserwuj w oknie konsoli zmianę stanów nowego zadania w zależności
     * od potomka.
     * 3. Odpowiedz na pytanie, czy istnieje sposób, aby (bez przechowywania
     * referencji do obiektu typu Task):
     * a) z zadania-potomka odwołać się do przodka ?
     * b) z zadania-przodka odwołać się do potomka ?
     * 4. Korzystając z zadań i funkcji zwrotnych, spróbuj odtworzyć funkcjonalność
     * operatora await w przykładzie z rodziału 1.
     */

    class Section6Zadania
    {
    }
}
