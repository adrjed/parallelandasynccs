﻿using AJ.ConsoleExtension;
using AJ.Reflection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelAndAsyncCS.MyTests
{
    [CInf(0,"Moje programy testowe")]
    class MyTests
    {
        [FInf(0,1, "Kontynuacja wątków")]
        public void Tasks_1()
        {
            Task<int> t1 = new Task<int>( () => 1 );
            Task<int> t2 = t1.ContinueWith( ( task ) => { Task.Delay( 1000 ).Wait(); return task.Result + 2; } );
            t1.Start();

            Console.WriteLine( "t1 = {0}, t2 = {1}",t1.Result, t2.Result);
        }

        [FInf(0,2,"Priorytety wątków")]
        public void Threads_2()
        {
            int a = 0;

            Thread highPriorT = new Thread( () =>
            {
                for ( int i = 0 ;i < 100 ;i++ ) 
                    ConsoleEx.WriteLine( "High priority:" + i + " a = " + a++, ConsoleColor.Green );
            } );
            Thread lowPriorT = new Thread( () => {
                for(int i = 0; i< 100; i++)
                    ConsoleEx.WriteLine("Low  priority:" + i + " a = " + a++, ConsoleColor.Yellow);
            } );

            highPriorT.Priority = ThreadPriority.AboveNormal;
            lowPriorT.Priority = ThreadPriority.Lowest;

            lowPriorT.Start();
            highPriorT.Start();
        }

        [FInf(0, 3, "Synchroniczne,asynchroniczne wykonywanie obliczen")]
        public void Threads_3()
        {
            const int tabSize = 120000;
            //int sum = 0;
            int[] bigTable = new int[tabSize];
            Stopwatch sw = new Stopwatch();

            Action<int, int> action = ( from, to ) =>
            {
                for ( int i = from ;i < to ;i++ )
                {
                    bigTable[i] = i;
                    bigTable[i] = (int)Math.Sin( bigTable[i] );
                }
            };

            // liczenie synchroniczne
            sw.Start();
            action( 0, tabSize );
            sw.Stop();
            Console.WriteLine( "Czas obliczeń sekwencyjnych: {0} ticks", sw.ElapsedTicks );

            // statyczna pętla równoległa TPL
            sw.Restart();
            Parallel.ForEach( bigTable, ( i, ls ) =>
            {
                bigTable[i] = i;
                bigTable[i] = (int)Math.Sin( bigTable[i] );
            } );
            sw.Stop();
            Console.WriteLine( "Czas obliczeń równoległych: {0} ticks", sw.ElapsedTicks );

            // ręczny podział zadania
            sw.Restart();
            Task t1 = new Task( () => action( 0, tabSize / 2 ) );
            Task t2 = new Task( () => action( tabSize / 2, tabSize ) );
            t1.Start(); t2.Start();
            t1.Wait(); t2.Wait();
            sw.Stop();
            Console.WriteLine( "Czas obliczeń ręcznie zrównoleglonych: {0} ticks", sw.ElapsedTicks );

            sw.Restart();
            t1 = Task.Factory.StartNew( () => action( 0, tabSize / 2 ) );
            t2 = Task.Factory.StartNew( () => action( 0, tabSize / 2 ) );
            t1.Wait(); t2.Wait();
            sw.Stop();
            Console.WriteLine( "Czas obliczeń task factory: {0} ticks", sw.ElapsedTicks );
        }

        [FInf(0,4,"Tasks test")]
        public void ExecutionContext_4()
        {
            Func<List<int>> getIntListAction = new Func<List<int>>( () =>
            {
                List<int> intList = new List<int>( 100 );
                for ( int i = 0 ;i < 100 ;i++ )
                    intList.Add( i );

                Task.Delay( 10 ).Wait();
                return intList;
            });

            Task<List<int>> t1 = new Task<List<int>>( () =>
            {
                return getIntListAction();
            } );

            t1.ContinueWith( ( task ) =>
            {
                task.Result.ForEach( i => ConsoleEx.WriteLine("t1: " + i.ToString(), ConsoleColor.Red ) );
            } );
            t1.Start();

            getIntListAction().ForEach( i => ConsoleEx.WriteLine("t2: " + i.ToString(), ConsoleColor.Green ) );
        }

        [FInf(0,5,"Test StringBuilder")]
        public void Test_5()
        {
            var sb = new StringBuilder();

            sb.Append( "Hello" );
            sb.Append( "," );
            sb.AppendLine( "World!" );

            Console.Write( sb.ToString() );
        }

        [FInf(0,6,"Task test")]
        public async void Test_6()
        {
            int x = 0;
            Stack<int> xStack = new Stack<int>();
            Random r = new Random();

            Action tstackPush = () =>
            {
                Console.WriteLine( "Uruchamiam tstackPush" );
                while ( true )
                {
                    x++;
                    if ( x % 5 == 0 )
                        xStack.Push( x );
                    Task.Delay( r.Next() % 50 ).Wait();
                }
            };

            Action tstackGet = () =>
            {
                Console.WriteLine( "Uruchamiam tstackGet" );
                while ( true )
                {
                    if ( xStack.Count >= 5 )
                    {
                        while(xStack.Count > 0)
                            ConsoleEx.WriteLine( string.Format( "Stack.Pop() = {0}", xStack.Pop() ), ConsoleColor.Blue );
                    }
                    else
                    {
                        ConsoleEx.WriteLine( "Stack.Count < 5. Waiting", ConsoleColor.Magenta );
                        Task.Delay( r.Next() % 200).Wait();
                    }
                }
            };

            Action taction = () =>
            {
                while ( true )
                {
                    x++;
                    Console.WriteLine( "Task\t {0}\t i = {1}", Task.CurrentId, x );
                    Task.Delay( 1000 ).Wait();
                }
            };

            Action tcheck = () =>
            {
                while ( true )
                {
                    if ( x % 5 == 0 )
                        ConsoleEx.WriteLine( string.Format( "!!! {0} % 5 == 0 !!!", x ), ConsoleColor.Red );
                    else
                        Console.WriteLine( "Waiting for x % 5 == 0..." );
                    Task.Delay( r.Next() % 500 + 2000 ).Wait();
                }
            };

            Func<Task<int>> fasync = () =>
            {
                return Task.Factory.StartNew<int>( () =>
                {
                    Task.Delay( 1000 ).Wait( );
                    ConsoleEx.WriteLine( "Koniec Task.Factory.StartNew<int>. Zwracam 2...", ConsoleColor.Green );
                    return 2;
                } );
            };

            Console.WriteLine( "Czekamy na fasync...");
            Console.WriteLine( "fasync zwróciło {0}", await fasync() );

            await Task.Factory.StartNew( tstackGet );
            await Task.Factory.StartNew( tstackPush );
            //Task[] tt = new Task[2];
            //Task tc = new Task( tcheck );
            //tc.Start();
            //Task.Delay( 2000 ).Wait();
            //for ( int i = 0 ;i < tt.Length ;i++ )
            //{
            //    tt[i] = new Task( taction );
            //    tt[i].Start();
            //}

        }

        [FInf(0,7,"Timer canceling a incrementing task")]
        public async void Test_7()
        {
            long bigInt = 0;

            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationTokenSource ctst = new CancellationTokenSource();
            Stopwatch sw = new Stopwatch();

            sw.Start();
            Thread thread = new Thread( ( o ) =>
            {
                CancellationToken ct = (CancellationToken)o;
                while ( !ct.IsCancellationRequested )
                    bigInt++;
            } );
            thread.Priority = ThreadPriority.Highest;

            thread.Start( ctst.Token );
            ctst.CancelAfter( 1000 );
            thread.Join();
            sw.Stop();
            Console.WriteLine( "Upłynęło: {0} ms", sw.ElapsedMilliseconds );
            Console.WriteLine( "Zwiekszana wartosc: {0}", bigInt );
            bigInt = 0;

            sw.Reset();

            Task task = new Task( ( o ) =>
            {
                CancellationToken ct = (CancellationToken)o;
                while ( !ct.IsCancellationRequested )
                    bigInt++;
            }, (object)cts.Token );

            task.Start();
            cts.CancelAfter( 1000 );
            sw.Start();
            await task.ContinueWith( ( tobj ) =>
            {
                if(tobj.Exception != null)
                {
                    tobj.Exception.Handle( ( ex ) =>
                    {
                        Console.WriteLine( "Wyjatek: {0}", ex.Message );
                        return true;
                    } );
                }
                Console.WriteLine( "Zakończono zadanie" );
                Console.WriteLine( "Status zadania: {0}", tobj.Status );
                Console.WriteLine( "Zwiekszana wartosc: {0}", bigInt );   
            } );
            sw.Stop();
            Console.WriteLine( "Upłynęło: {0} ms", sw.ElapsedMilliseconds);
        }

        public async void Test8Task(object o)
        {
            await Task.Delay( 1000 );
            int i = (int)o;
            i--;
            Console.WriteLine( "Wewnątrz Test8Task int i = {0}", i );
        }

        internal class Interval
        {
            public Interval(int value)
            {
                this.value = value;
            }
            public int value;

            public override string ToString()
            {
                return value.ToString();
            }
        }

        internal event TimerCallback timerTickEvent;

        [FInf(0, 8, "Timer in other thread")]
        public void Test_8()
        {
            Interval interval = new Interval( 1000 );
            object[] objects = new object[2];
            timerTickEvent += TimerTick;
            Timer timer = new Timer( timerTickEvent, objects, interval.value, interval.value );
            objects[0] = interval;
            objects[1] = timer;      
        }

        public void ChangeInterval(Timer t, Interval i)
        {
            i.value -= 50;
            t.Change( i.value, i.value );
            Console.Write( " Interval changed to: {0}\n", i );
        }

        public void TimerTick(object obj)
        {
            object[] objects = (object[])obj;
            Interval interval = (Interval)objects[0];
            Timer timer = (Timer)objects[1];
            Console.Write( "Tick!" );
            ChangeInterval( timer, interval );
        }

    }
}