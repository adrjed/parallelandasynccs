﻿using System;
using System.Threading;
using System.Windows;

using AJ.ConsoleExtension;
using AJ.Reflection;

namespace ParallelAndAsyncCS
{
	public class ParallelAndAsyncCSConsole : ReflectionConsole
	{
		protected override void AboutCommand()
		{
			System.Console.WriteLine( "Parallel and Async CS examples - Adrian Jędrzejak" );
		}
	}

	public static class ConsoleMain
	{
		static ReflectionConsole rConsole = new ParallelAndAsyncCSConsole();

		[STAThread]
		public static void Main()
		{
			rConsole.Run();
		}

		private static void KeysTopBarInfo()
		{
			string str = "";
			bool sw = false;
			ConsoleColor keyColor;
			Array colors = Enum.GetValues( typeof(ConsoleColor) );
			Point curPos = new Point();
			Point topPos = new Point( 0, 0 );
			Random r = new Random();
			int i = 0;

			while ( true )
			{
				if ( Console.KeyAvailable || true )
				{
					curPos.X = Console.CursorLeft;
					curPos.Y = Console.CursorTop;
					//Console.SetCursorPosition( (int)topPos.X, (int)topPos.Y );
					Console.SetCursorPosition( Console.BufferWidth / 2, Console.WindowHeight / 2 );
					keyColor = (ConsoleColor)colors.GetValue( ++i % colors.Length );

					str = (sw = !sw) ? "(o )(o )" : "( o)( o)";

					ConsoleEx.Write( str, keyColor );

					topPos.X = Console.CursorLeft;
					topPos.Y = Console.CursorTop;
					Console.SetCursorPosition( (int)curPos.X, (int)curPos.Y );
				}
				Thread.Sleep( 100 );
			}
		}
	}

}

